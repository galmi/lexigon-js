cc.game.onStart = function () {
    //cc.view.setDesignResolutionSize(1024, 768, cc.ResolutionPolicy.SHOW_ALL);
    //cc.view.resizeWithBrowserSize(false);
    //load resources
    cc.LoaderScene.preload(g_menu, function () {
        //cc.UserData['gamesPlayed'] = 0;
        //cc.UserData['paid'] = true;
        cc.eventManager.addCustomListener(EVENT_TUTORIAL_END, function() {
            cc.UserData['gamesPlayed'] = cc.UserData['gamesPlayed'] || 1;
        });
        cc.director.runScene(MainMenu.scene());
    }, this);
};
cc.game.run();