var LANG = {
    ru: {
        'End game': 'Закончить',
        'Continue': 'Продолжить',
        'Main menu': 'Главное меню',
        'New game': 'Новая игра',
        'Results': 'Результаты',
        'Won': 'Победа',
        'Lose': 'Поражение',
        'Words found': "Слов\nнайдено",
        'Score': "Очков\nнабрано",
        'Played': 'Сыграно',
        'games': 'игр',
        'found': 'нашел',
        'Time spent': "Время\nзатрачено",
        'Time spent, sec': 'Время\nигры, сек',
        'Survive': 'Выживание',
        'Escape': 'Спасение',
        'Explore': 'Изучение',
        'Statistic': 'Статистика',
        'Tutorial': 'Обучение',
        'Best words': 'Лучшие слова',
        'Global statistic': 'Глобальная статистика',
        'Best of friends': 'Статистика друзей',
        'Self statistic': 'Личная статистика',
        "Best\nwords": 'Лучшие\nслова',
        "Global\nstatistic": 'Глобальная\nстатистика',
        "Best of\nfriends": 'Статистика\nдрузей',
        "Self\nstatistic": 'Личная\nстатистика',
        'Our group': 'Вопросы и пожелания в нашей группе',
        'Choose dictionary': 'Выбери словарь',
        'Russian': 'Русский',
        'English': 'Английский',
        'Dictionary': 'Словарь',
        'Do you want end the game?': 'Завершить игру?',
        'I %s word "%s" in game Lexigon. What will you find?': 'Я %s слово "%s" в игре Лексигон.\nА что найдешь ты!?',
        'I %s word "%s"': 'Я %s слово "%s"',
        'The word must touch a cleared tile': 'Слово должно касаться пустой ячейки',
        'The word must touch a cleared tile. Select word "%s"': 'Любое слово должно касаться пустой ячейки\nЗажми курсор на первой букве и выдели слово \"%s\"',
        'Click to a cleared tile and drag mouse to scroll. Try for continue': 'Зажми курсором пустую ячейку в центре и перемещай поле\nПопробуй, чтобы продолжить',
        'Hexabombs burn nearest tiles. Select "%s"': 'Гексабомбы взрывают ближние ячейки\nВыдели слово \"%s\"',
        'Select word "%s" for finish tutorial': 'Дойди до края, чтобы закончить обучение\nВыдели слово \"%s\"',
        'Tutorial completed': 'Обучение завершено',
        'Begin from middle': 'Всегда начинай с середины поля',
        'Each word must touch a cleared tile': 'Любое слово должно касаться пустой ячейки',
        'Click to a cleared tile and drag mouse to scroll': 'Кликни на пустую ячейку, чтобы перемещать игровое поле',
        'Use hexabombs for burn nearest tiles': 'Используй гексабомбы, чтобы взорвать ближайшие ячейки',
        'Share found words with your friends': 'Делись с друзьями найденными словами',
        'Get in the TOP-10 players by earning more points': 'Попади в ТОП-10 игроков, зарабатывая больше очков',
        'Time limited game. Find exit as soon as possible.': 'Игра на время. Найди выход как можно быстрее.',
        'The game will be unlocked after Tutorial.': 'Разблокируется после Обучения.',
        'The game without limits.': 'Игра без ограничения времени.',
        'Find the largest number of words.': 'Найди наибольшее число слов.',
        'Unlocked after %d games played, or after payment %s. Click for pay.': 'Разблокируется после %d игр или после оплаты %s. Нажми для оплаты.',
        'The game against time. Find words for survive.': 'Игра против времени. Найди слова, чтобы выжить.',
        'Play Tutorial game first.': 'Пройдите обучение перед началом игры.',
        'Lexigon': 'Лексигон',
        'You completed Tutorial': 'Ты закончил обучение',
        'Achievements': 'Награды',
        'You made %s letters word': 'Найдено слово из %s букв',
        'You played %s games': 'Сыграно %s игр',
        'You used a hexabomb': 'Использована гексабомба',
        'Make a word that uses 2 hexabombs': 'Найдено слово с двумя гексабомбами',
        'You exploded a hexabomb with another hexabomb': 'Взорвана гексабомба другой\nгексабомбой',
        'Find the same 4+ letters word consecutively': 'Найдено одно слово из 4+ букв\nподряд',
        'You escaped for the first time': 'Ты спасся в первый раз',
        'You escaped in under 2 minutes': 'Спасение за 2 минуты',
        'You escaped in under 1 minute': 'Спасение за 1 минуту',
        'You escaped in under %s seconds': 'Спасение за %s секунд',
        'You escaped %s times in a row': 'Спасение подряд %s раз',
        'You escaped using 5+ letters word': 'Спасение со словом из 5+ букв',
        'You escaped using hexabomb': 'Спасение с использованием\nгексабомбы',
        'Score %s in Explore mode': 'В Изучении набрано %s очков',
        'You survived for 1 minute': 'Выживание в течение 1 минуты',
        'You survived for %s minutes': 'Выживание в течение %s минут',
        'Bump your remaining time to 60 seconds in Survive mode': 'Увеличен таймер в Выживании до\n60 секунд',
        'Your score': 'Набрано очков',
        'New Achievement': 'Новое достижение',
        'Remove AD': 'Убрать рекламу',
        'Unlock all for %s': 'Разблокировать всё за %s',
        'The Graduate': 'Образован',
        '%d letters word': 'Слово из %d букв',
        'The %d games': '%d игр',
        'Hexabomber': 'Гексабомбер',
        'The Big Bang': 'Большой БУМ',
        'Drive By Hexabomber': 'Гексабомбер 2',
        'Four Plus Four': '4 плюс 4',
        'You Escaped': 'Выбрался',
        'Two Minutes Escape': 'Выбрался за 2мин',
        'One Minute Escape': 'Выбрался за 1мин',
        '%d Seconds Escape': 'Выбрался за %dсек',
        '%d Games Strike': '%d страйков',
        'High Society': 'Грамотный',
        'Hexagone': 'Гексагон',
        'The %dK Club': '%dК клуб',
        'The %dM Club': '%dМ клуб',
        'Smashing it': 'Превосходный',
        'Language': 'English',
        'Hinted word "%s"': 'Подсказка "%s"',
        'Buy hints': 'Купить подсказки',
        'Buy %d hints for %d%s': '%d шт. за %d %s'
    },
    en: {
        'I found word "%s" in game Lexigon. What will you find?': "I found word \"%s\" in game Lexigon.\nWhat will you find?",
        'The word must touch a cleared tile. Select word "%s"': "The word must touch a cleared tile.\nSelect word \"%s\"",
        'Click to a cleared tile and drag mouse to scroll. Try for continue': "Click to a cleared tile and drag mouse to scroll.\nTry for continue",
        'Hexabombs burn nearest tiles. Select "%s"': "Hexabombs burn nearest tiles.\nSelect word \"%s\"",
        'You exploded a hexabomb with another hexabomb': 'You exploded a hexabomb\nwith another hexabomb',
        'Find the same 4+ letters word consecutively': 'Find the same 4+ letters word\nconsecutively',
        'Bump your remaining time to 60 seconds in Survive mode': 'Bump your remaining time to 60 seconds\nin Survive mode',
        'Language': 'Русский'
    }
};

function t(message) {
    var lang = cc.UserData['lang'];
    if (LANG.hasOwnProperty(lang) && LANG[lang][message]) {
        message = LANG[lang][message];
    }
    if (arguments.length > 1) {
        delete (arguments);
        var tmp = [];
        for (var i in arguments) {
            if (i > 0)
                tmp.push(arguments[i]);
        }
        return vsprintf(message, tmp);
    }
    return message;
}

function setUserLang() {
    var lang = getCookie('lang');
    if (lang) {
        cc.UserData['lang'] = lang;
    } else {
        if (navigator.language && navigator.language.toLowerCase().indexOf('en') > -1) {
            cc.UserData['lang'] = 'en';
        } else {
            cc.UserData['lang'] = 'ru';
        }
        setCookie('lang', cc.UserData['lang'], 180);
    }
}