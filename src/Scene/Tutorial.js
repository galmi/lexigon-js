var SceneTutorial = SceneGameAbstract.extend({
    gameLogic: GameTutorial,
    infoLayer: null,
    init: function () {
        this._class = SceneTutorial;
        try {
            ga('send', 'event', 'game', 'tutorial', 'begin');
        } catch (e) {
        }
        this.startQuest(1);
    },
    createInfoBox: function (text) {
        if (this.infoLayer) {
            this.infoLayer.removeFromParent(true);
            this.infoLayer = null;
        }
        var size = cc.director.getWinSize();
        var delta = 10;
        this.infoLayer = cc.LayerColor.create(cc.color(0, 0, 0, 255), size.width, 60);
        this.infoLayer.setOpacity(200);
        this.infoLayer.setPositionX(0);
        this.infoLayer.setPositionY(size.height - this.infoLayer.height);
        var label = new cc.LabelTTF(text, "Intro", 20, cc.size(size.width, 60), cc.TEXT_ALIGNMENT_CENTER, cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
        label.setFontFillColor(res.colors.tutorial);
        this.infoLayer.addChild(label);
        label.x = label.width / 2 + delta;
        label.y = this.infoLayer.height / 2;

        this.addChild(this.infoLayer);
        label.runAction(cc.sequence(cc.fadeOut(.3), cc.fadeIn(.3)));
    },

    startQuest: function (questNumber) {
        if (questNumber === 1) {
            this.quest1();
            try {
                ga('send', 'event', 'game', 'tutorial', 'quest1');
            } catch (e) {
            }
        } else if (questNumber === 2) {
            this.quest2();
            try {
                ga('send', 'event', 'game', 'tutorial', 'quest2');
            } catch (e) {
            }
        } else if (questNumber === 3) {
            this.quest3();
            try {
                ga('send', 'event', 'game', 'tutorial', 'quest3');
            } catch (e) {
            }
        } else if (questNumber === 4) {
            this.quest4();
            try {
                ga('send', 'event', 'game', 'tutorial', 'quest4');
            } catch (e) {
            }
        }
    },

    quest1: function () {
        var tutorialWord = getDictValue('tutorialQuest1word');
        Dict = [tutorialWord.toLowerCase()];
        this.createInfoBox(t('The word must touch a cleared tile. Select word "%s"', tutorialWord));
        var action = getDictValue('tutorialQuest1Action');
        action.call(this);
        var listener = cc.eventManager.addCustomListener(EVENT_END_WORD, function () {
            var word = this.game.word;
            if (word == tutorialWord) {
                this.startQuest(2);
                cc.eventManager.removeListener(listener);
            }
        }.bind(this));
    },

    quest2: function () {
        this.createInfoBox(t('Click to a cleared tile and drag mouse to scroll. Try for continue'));
        var listener = cc.eventManager.addCustomListener(EVENT_TOUCH_MOVED, function () {
            this.startQuest(3);
            cc.eventManager.removeListener(listener);
        }.bind(this));
    },

    quest3: function () {
        var tutorialWord = getDictValue('tutorialQuest3word');
        Dict = [tutorialWord.toLowerCase()];
        this.createInfoBox(t('Hexabombs burn nearest tiles. Select "%s"', tutorialWord));
        var action = getDictValue('tutorialQuest3Action');
        action.call(this);
        var listener = cc.eventManager.addCustomListener(EVENT_END_WORD, function () {
            var word = this.game.word;
            if (word == tutorialWord) {
                this.startQuest(4);
                cc.eventManager.removeListener(listener);
            }
        }.bind(this));
    },

    quest4: function () {
        var tutorialWord = getDictValue('tutorialQuest4word');
        Dict = [tutorialWord.toLowerCase()];
        this.createInfoBox(t('Select word "%s" for finish tutorial', tutorialWord));
        var action = getDictValue('tutorialQuest4Action');
        action.call(this);
        var listener = cc.eventManager.addCustomListener(EVENT_END_WORD, function () {
            var word = this.game.word;
            if (word == tutorialWord) {
                cc.eventManager.removeListener(listener);
                this.infoLayer.removeFromParent(true);
                cc.eventManager.dispatchCustomEvent(EVENT_GAME_END, t('Tutorial completed'));
            }
        }.bind(this));
    },

    showResults: function () {
        this.removeAllChildren(true);
        cc.eventManager.dispatchCustomEvent(EVENT_TUTORIAL_END);
        var resultsData = {
            title: t('Tutorial completed'),
            newGameButton: false
        };
        var size = cc.director.getWinSize();
        var background = new cc.Sprite(res.Background_png);
        background.x = size.width / 2;
        background.y = size.height / 2;
        this.addChild(background);
        this.createResultLayer(resultsData);

        var tutorialMessage = "• " + t('Begin from middle') + "\n" +
            "• " + t('Each word must touch a cleared tile') + "\n" +
            "• " + t('Click to a cleared tile and drag mouse to scroll') + "\n" +
            "• " + t('Use hexabombs for burn nearest tiles') + "\n" +
            "• " + t('Share found words with your friends') + "\n" +
            "• " + t('Get in the TOP-10 players by earning more points');
        var tutorialMessageLabel = new cc.LabelTTF(tutorialMessage, 'Sans', 24);
        tutorialMessageLabel.setPosition(size.width / 2, size.height / 2 + tutorialMessageLabel.height / 2);
        this.addChild(tutorialMessageLabel);

        try {
            ga('send', 'event', 'game', 'tutorial', 'finish');
        } catch (e) {
        }
    }
});