var MainMenu = cc.Layer.extend({
    gameButtons: [],
    listeners: [],
    init: function () {
        changeRes(winSize.w, winSize.h);
        cc.spriteFrameCache.addSpriteFrames(res.Menu_plist);
        cc.spriteFrameCache.addSpriteFrames(res.Achieve_plist);
        var size = cc.director.getWinSize();

        var background = new cc.Sprite(res.Background_png);
        background.x = size.width / 2;
        background.y = size.height / 2;
        this.addChild(background);

        var title = new cc.LabelTTF(t('Lexigon'), 'Intro', 90);
        title.setFontFillColor(cc.color(214, 213, 213));
        title.x = size.width / 2;
        title.y = size.height - title.height / 2 - 20;
        this.addChild(title)

        var items = [];

        if (!cc.UserData['gamesPlayed']) {
            items.push(this.getMenuItem('tutorial', function () {
                cc.LoaderScene.preload(getGameResources(), function () {
                    Dict = [];
                    LETTERS_FREQ = {};
                    BOMB_LETTERS = ['Х'];
                    var scene = new SceneTutorial();
                    cc.director.runScene(scene);
                }, this);
            }.bind(this)));
        } else {
            items.push(this.getMenuItem('survive', function () {
                this.checkStart(SceneSurvive, GameSurvive.gamesLimit);
            }.bind(this), GameSurvive.gamesLimit));
            items.push(this.getMenuItem('escape', function () {
                this.gameStart(SceneEscape, GameEscape.gamesLimit);
            }.bind(this), GameEscape.gamesLimit));
            items.push(this.getMenuItem('explore', function () {
                this.checkStart(SceneExplore, GameExplore.gamesLimit);
            }.bind(this), GameExplore.gamesLimit));
        }
        var menu = cc.Menu.create(items);
        cc.eventManager.addListener({
            event: cc.EventListener.MOUSE,
            onMouseMove: function (event) {
                var target = event.getCurrentTarget();
                var currentItem = target._itemForTouch(event);
                if (currentItem != target._selectedItem) {
                    if (target._selectedItem)
                        target._selectedItem.unselected();
                    target._selectedItem = currentItem;
                    if (target._selectedItem)
                        target._selectedItem.selected();
                }
            }.bind(menu)
        }, menu);
        menu.alignItemsHorizontallyWithPadding(50);
        this.addChild(menu);
        menu.x = size.width / 2;
        menu.y = size.height / 2 + 80;

        this.descriptionLayer = new cc.Layer();
        //this.titleSprite = new cc.Sprite(res.Escape_title_png);
        this.descriptionLayer.x = size.width / 2;
        this.descriptionLayer.y = size.height / 2 - 40;
        this.addChild(this.descriptionLayer);

        if (!cc.UserData['gamesPlayed']) {
            items[items.length - 1].selected();
        } else {
            items[1].selected();
        }
        this.listeners.push(cc.eventManager.addCustomListener(EVENT_LOADED_STAT, function () {
            this.addStatMenu();
        }.bind(this)));
        if (cc.Statistic.hasOwnProperty('user')) {
            this.addStatMenu();
        }

        this.addGroupLink();
        this.addDictionaryButton();
        if (cc.screen._supportsFullScreen) {
            this.addFullscreenButton();
        }

        this.listeners.push(cc.eventManager.addCustomListener(EVENT_MAIN_MENU_ADD_CHILD, function (data) {
            try {
                var cmp = data.getUserData();
                this.addChild(cmp);
            } catch (e) {
                console.log('[ERROR]', e);
            }
        }.bind(this)));

        cc.eventManager.dispatchCustomEvent(EVENT_MAIN_MENU_LOADED);

        return true;
    },
    addStatMenu: function () {
        var size = cc.director.getWinSize();
        var statMenu = this.getStatisticMenu();
        this.addChild(statMenu);
        statMenu.x = size.width / 2;
        statMenu.y = 140;
    },
    cleanup: function () {
        this._super();
        for (var i in this.listeners) {
            cc.eventManager.removeListener(this.listeners[i]);
        }
    },
    resetButtons: function () {
        for (var i in this.gameButtons) {
            var gameButton = this.gameButtons[i];
            gameButton.isActive = false;
            gameButton.setNormalImage(gameButton.normalSprite);
        }
    },
    checkStart: function (gameScene, gameLimit) {
        var gamePlayed = cc.UserData['gamesPlayed'] || 0;
        if (!isPaid() && parseInt(gamePlayed) < gameLimit) {
            //var lockedLayer = LockedLayer.create();
            //lockedLayer.x = lockedLayer.width/2;
            //lockedLayer.y = lockedLayer.height/2;
            //this.addChild(lockedLayer);
            cc.eventManager.dispatchCustomEvent(EVENT_PAY);
            try {
                ga('send', 'event', 'pay', 'init');
            } catch (e) {
            }
        } else {
            this.gameStart(gameScene);
        }
    },
    gameStart: function (game) {
        cc.LoaderScene.preload(getGameResources(), function () {
            Dict = cc.loader.getRes(getDictValue('dict'));
            LETTERS_FREQ = getDictValue('lettersFreq');
            BOMB_LETTERS = getDictValue('bombLetters');
            var scene = new game();
            cc.director.runScene(scene);
        }, this);
    },
    getMenuItem: function (button, startCallback, checkPayment) {
        var normal = 'normal';
        var selected = 'selected';
        var description = 'description';
        var gamePlayed = cc.UserData['gamesPlayed'] || 0;
        if (checkPayment && (!isPaid() && parseInt(gamePlayed) < checkPayment)) {
            normal += '_locked';
            selected += '_locked';
            description += '_locked';
        }
        var menuItemSprite = this.getMenuItemSprite(button, normal);
        var item = cc.MenuItemSprite.create(
            menuItemSprite,
            this.getMenuItemSprite(button, selected),
            null,
            function (cmp) {
                startCallback();
            }.bind(this)
        );
        item.spriteName = button;
        item.normalSprite = menuItemSprite;
        item.selected = function () {
            var size = cc.director.getWinSize();
            cc.MenuItem.prototype.selected.call(this);
            this.resetButtons();
            item.setNormalImage(this.getMenuItemSprite(button, selected));
            //item.isActive = true;
            if (this.descriptionLayer) {
                this.descriptionLayer.removeAllChildren(true);
            }
            //var titleSprite = new cc.Sprite();
            //titleSprite.initWithSpriteFrameName(button + '_title.png');
            var titleLabel = new cc.LabelTTF(res[button + '_title'], "Intro", 68);
            titleLabel.y = -titleLabel.height / 2;
            this.descriptionLayer.addChild(titleLabel);
            var descriptionLabel = new cc.LabelTTF(res[button + '_' + description], "Arial", 20, cc.size(size.width, 50), cc.TEXT_ALIGNMENT_CENTER);
            descriptionLabel.y = titleLabel.y - titleLabel.height / 2 - descriptionLabel.height / 2;
            this.descriptionLayer.addChild(descriptionLabel);
        }.bind(this);
        this.gameButtons.push(item);
        return item;
    },
    /**
     * @param button ()
     * @param type ('normal', 'selected')
     * @returns {cc.Sprite}
     */
    getMenuItemSprite: function (button, type) {
        var spriteFrameNameNormal = "#" + button + "_" + type + ".png";
        var surviveNormal = new cc.Sprite(spriteFrameNameNormal);
        var spriteFrameNameSelected;
        if (type == 'selected') {
            spriteFrameNameSelected = "#play_icon.png";
        } else {
            spriteFrameNameSelected = "#" + button + "_icon.png";
        }
        var surviveIcon = new cc.Sprite(spriteFrameNameSelected);
        surviveIcon.x = surviveNormal.width / 2;
        surviveIcon.y = surviveNormal.height / 2;
        surviveNormal.addChild(surviveIcon);
        return surviveNormal;
    },
    getStatisticMenu: function () {
        var sprite, spriteSelected;
        var items = [];
        var text;

        if (cc.Statistic.hasOwnProperty('user')) {
            sprite = new cc.Sprite('#stat_self_normal.png');
            spriteSelected = new cc.Sprite('#stat_self_selected.png');
            var user = new cc.MenuItemSprite(
                sprite,
                spriteSelected,
                null,
                function () {
                    var scene = new StatisticScene(cc.Statistic.user, t('Self statistic'));
                    cc.director.runScene(scene);
                }.bind(this)
            );
            text = new cc.LabelTTF(t("Self\nstatistic"), 'Arial', 12, null, cc.TEXT_ALIGNMENT_CENTER);
            text.setPosition(sprite.width / 2, -text.height / 2 - 5);
            user.addChild(text);
            items.push(user);
        }

        if (cc.Statistic.hasOwnProperty('friends')) {
            sprite = new cc.Sprite('#stat_friends_normal.png');
            spriteSelected = new cc.Sprite('#stat_friends_selected.png');
            var users = new cc.MenuItemSprite(
                sprite,
                spriteSelected,
                null,
                function () {
                    var scene = new StatisticScene(cc.Statistic.friends, t('Best of friends'));
                    cc.director.runScene(scene);
                }.bind(this)
            );
            text = new cc.LabelTTF(t("Best of\nfriends"), 'Arial', 12, null, cc.TEXT_ALIGNMENT_CENTER);
            text.setPosition(sprite.width / 2, -text.height / 2 - 5);
            users.addChild(text);
            items.push(users);
        }

        if (cc.Statistic.hasOwnProperty('globe')) {
            sprite = new cc.Sprite('#stat_global_normal.png');
            spriteSelected = new cc.Sprite('#stat_global_selected.png');
            var globe = new cc.MenuItemSprite(
                sprite,
                spriteSelected,
                null,
                function () {
                    var scene = new StatisticScene(cc.Statistic.globe, t('Global statistic'));
                    cc.director.runScene(scene);
                }.bind(this)
            );
            text = new cc.LabelTTF(t("Global\nstatistic"), 'Arial', 12, null, cc.TEXT_ALIGNMENT_CENTER);
            text.setPosition(sprite.width / 2, -text.height / 2 - 5);
            globe.addChild(text);
            items.push(globe);
        }

        if (cc.Statistic.hasOwnProperty('words')) {
            sprite = new cc.Sprite('#stat_words_normal.png');
            spriteSelected = new cc.Sprite('#stat_words_selected.png');
            var words = new cc.MenuItemSprite(
                sprite,
                spriteSelected,
                null,
                function () {
                    var scene = new WordsStatisticScene(cc.Statistic['words'], t('Best words'));
                    cc.director.runScene(scene);
                }.bind(this)
            );
            text = new cc.LabelTTF(t("Best\nwords"), 'Arial', 12, null, cc.TEXT_ALIGNMENT_CENTER);
            text.setPosition(sprite.width / 2, -text.height / 2 - 5);
            words.addChild(text);
            items.push(words);
        }

        sprite = new cc.Sprite('#achievement_normal.png');
        spriteSelected = new cc.Sprite('#achievement_selected.png');
        var achieve = new cc.MenuItemSprite(
            sprite,
            spriteSelected,
            null,
            function () {
                var scene = new AchievementsScene();
                var backgroundScene = new BackgroundScene(scene);
                cc.director.runScene(backgroundScene);
            }.bind(this)
        );
        text = new cc.LabelTTF(t("Achievements"), 'Arial', 12, null, cc.TEXT_ALIGNMENT_CENTER);
        text.setPosition(sprite.width / 2, -text.height / 2 - 5);
        achieve.addChild(text);
        items.push(achieve);

        var menu = cc.Menu.create(items);
        menu.alignItemsHorizontallyWithPadding(100);
        return menu;
    },

    addGroupLink: function () {
        if (typeof GROUP_URL !== 'undefined') {
            var message = t('Our group') + ' ';
            var messageLabel = new cc.LabelTTF(message, 'Arial', 18);
            var url = GROUP_URL;
            if (typeof GROUP_NAME !== 'undefined') {
                url = GROUP_NAME;
            }
            var urlLabel = new cc.LabelTTF(url, 'Arial', 22);
            var width = messageLabel.width + urlLabel.width;
            var height = 20;

            var size = cc.director.getWinSize();
            messageLabel.x = size.width / 2 - width / 2 + messageLabel.width / 2;
            messageLabel.y = height;
            this.addChild(messageLabel);
            urlLabel.x = messageLabel.x + messageLabel.width / 2 + urlLabel.width / 2;
            urlLabel.y = height;
            this.addChild(urlLabel);

            cc.eventManager.addListener({
                event: cc.EventListener.TOUCH_ONE_BY_ONE,
                onTouchBegan: function (touch, event) {
                    var target = event.getCurrentTarget();
                    var locationInNode = target.convertToNodeSpace(touch.getLocation());
                    var s = target.getContentSize();
                    var rect = cc.rect(0, 0, s.width, s.height);

                    if (cc.rectContainsPoint(rect, locationInNode)) {
                        window.open('//' + GROUP_URL, '_blank');
                        return true;
                    }
                    return false;
                }
            }, urlLabel);
            urlLabel.gameCanvasCursor = document.getElementById('gameCanvas');
            cc.eventManager.addListener({
                event: cc.EventListener.MOUSE,
                onMouseMove: function (event) {
                    var target = event.getCurrentTarget();
                    var locationInNode = target.convertToNodeSpace(event.getLocation());
                    var s = target.getContentSize();
                    var rect = cc.rect(0, 0, s.width, s.height);

                    if (cc.rectContainsPoint(rect, locationInNode)) {
                        this.gameCanvasCursor.style.cursor = 'pointer';
                        return true;
                    }
                    this.gameCanvasCursor.style.cursor = 'auto';
                    return false;
                }.bind(urlLabel)
            }, urlLabel);

        }
    },

    addDictionaryButton: function () {
        var winSize = cc.director.getWinSize();
        var items = [];
        var flagSprite, flagSprite2;
        if (cc.UserData['lang'] == 'ru') {
            flagSprite = new cc.Sprite('#en_flag.png');
            flagSprite2 = new cc.Sprite('#en_flag.png');
        } else {
            flagSprite = new cc.Sprite('#ru_flag.png');
            flagSprite2 = new cc.Sprite('#ru_flag.png');
        }
        var flagItem = new cc.MenuItemSprite(flagSprite, flagSprite2, null, function () {
            if (cc.UserData['lang'] == 'en') {
                setCookie('lang', 'ru', 180);
            } else {
                setCookie('lang', 'en', 180);
            }
            setUserLang();
            document.location.reload();
        }.bind(this));
        var flagLabel = new cc.LabelTTF(t('Language'), 'Arial', 12);
        flagLabel.setPositionX(flagItem.width/2);
        flagLabel.setPositionY(-5);
        flagItem.addChild(flagLabel);
        items.push(flagItem);

        var dictIconSprite = new cc.Sprite('#dictionary.png');
        var dictItem = new cc.MenuItemSprite(dictIconSprite, new cc.Sprite('#dictionary.png'), null, function () {
            this.onExit();
            var dictMenu = DictionaryMenuLayer.create();
            this.addChild(dictMenu);
            dictMenu.onEnter();
        }.bind(this));
        var dictTitle = new cc.LabelBMFont(getDictValue('title'), res.FntIntro20White);
        dictTitle.width = 25;
        dictTitle.setPositionX(dictItem.width / 2 + dictTitle.width);
        dictTitle.setPositionY(dictIconSprite.width / 2);
        dictItem.width = dictItem.width + dictTitle.width;
        dictItem.addChild(dictTitle);
        items.push(dictItem);
        var dictLabel = new cc.LabelTTF(t('Dictionary'), 'Arial', 12);
        dictLabel.setPositionX(dictItem.width/2);
        dictLabel.setPositionY(-5);
        dictItem.addChild(dictLabel);

        var menu = new cc.Menu(items);
        menu.setPosition(winSize.width / 2, 65);
        menu.alignItemsHorizontallyWithPadding(50);
        this.addChild(menu);
    },
    addFullscreenButton: function () {
        var winSize = cc.director.getWinSize();
        var fullScreenIconSprite = new cc.Sprite('#fullscreen_normal.png');
        var fullScreenItem = new cc.MenuItemSprite(fullScreenIconSprite, new cc.Sprite('#fullscreen_selected.png'), null, function () {
            cc.screen.requestFullScreen(document.getElementById('gameCanvas'));
        }.bind(this));
        var menu = new cc.Menu(fullScreenItem);
        menu.setPosition(winSize.width - fullScreenIconSprite.width / 2, winSize.height - fullScreenIconSprite.height / 2);
        this.addChild(menu);
    }
});

MainMenu.create = function () {
    var sg = new MainMenu();
    if (sg && sg.init()) {
        return sg;
    }
    return null;
};

MainMenu.scene = function (hideAdvert) {
    var scene = cc.Scene.create();
    var layer = MainMenu.create();
    scene.addChild(layer);
    return scene;
};
