var WordsStatisticScene = cc.Scene.extend({
    ctor: function (data, statType) {
        this._super();
        changeRes(winSize.w, winSize.h);
        var size = cc.director.getWinSize();
        var i, index, rowLayer, numLabel, scoreLabel, userLabel;
        var padding = 20;

        var background = new cc.Sprite(res.Background_png);
        background.x = size.width / 2;
        background.y = size.height / 2;
        this.addChild(background);

        var titleLabel = new cc.LabelTTF(statType, "Intro", 50);
        titleLabel.setColor(cc.color(213, 213, 213, 255));
        titleLabel.setPositionX(size.width / 2);
        titleLabel.setPositionY(size.height - titleLabel.height / 2 - padding);
        this.addChild(titleLabel);

        var menuItemSprite = new cc.Sprite('#' + res.sprite.menuNormal);
        var mainMenuButton = new cc.MenuItemSprite(
            menuItemSprite,
            new cc.Sprite('#' + res.sprite.menuSelected),
            null,
            function () {
                var scene = MainMenu.scene();
                cc.director.runScene(scene);
            }
        );
        var mainMenuText = new cc.LabelBMFont(t('Main menu'), res.FntIntro25Gray);
        //mainMenuText.setColor(cc.color(50, 65, 68, 255));
        mainMenuText.x = menuItemSprite.width / 2;
        mainMenuText.y = menuItemSprite.height / 2;
        mainMenuButton.addChild(mainMenuText);
        var menu = new cc.Menu([mainMenuButton]);
        menu.width = mainMenuButton.width;
        menu.height = mainMenuButton.height;
        menu.setPositionY(titleLabel.y - titleLabel.height / 2 - padding * 2);
        this.addChild(menu);

        var newY = menu.y - menu.height / 2 - padding * 2;

        var lineHoriz = new cc.Sprite('#' + 'line_horiz.png');
        lineHoriz.x = size.width / 2;
        lineHoriz.y = newY;
        this.addChild(lineHoriz);

        newY = lineHoriz.y - padding;
        var columnWidth = lineHoriz.width / 2;
        var rowHeight = 23;

        var posX = lineHoriz.x;// - lineHoriz.width / 2;
        var items = [];
        for (i in data) {
            index = parseInt(i) + 1;
            rowLayer = new cc.Node();
            rowLayer.width = lineHoriz.width;
            numLabel = new cc.LabelTTF(index + '.', "Intro", 18, cc.size(columnWidth / 2, rowHeight), cc.TEXT_ALIGNMENT_LEFT);
            numLabel.setFontFillColor(cc.color(153, 153, 153, 255));
            numLabel.setPosition(0, 0);
            rowLayer.addChild(numLabel);
            scoreLabel = new cc.LabelTTF(data[i].value, "Intro", 18, cc.size(columnWidth, rowHeight), cc.TEXT_ALIGNMENT_LEFT);
            scoreLabel.setFontFillColor(cc.color(213, 213, 213, 255));
            scoreLabel.setPosition(112, 0);
            rowLayer.addChild(scoreLabel);
            userLabel = new cc.LabelTTF(data[i].first_name + ' ' + data[i].last_name, "Arial", 18, cc.size(columnWidth, rowHeight), cc.TEXT_ALIGNMENT_LEFT);
            userLabel.setFontFillColor(res.colors.tutorial);
            userLabel.setPosition(columnWidth / 2 - userLabel.width / 4 + 28, -userLabel.height+2);
            rowLayer.addChild(userLabel);
            if (data[i].link) {
                userLabel.userLink = data[i].link;
                cc.eventManager.addListener({
                    event: cc.EventListener.TOUCH_ONE_BY_ONE,
                    onTouchBegan: function (touch, event) {
                        var target = event.getCurrentTarget();
                        var locationInNode = target.convertToNodeSpace(touch.getLocation());
                        var s = target.getContentSize();
                        var rect = cc.rect(0, 0, s.width, s.height);

                        if (cc.rectContainsPoint(rect, locationInNode)) {
                            window.open(this.userLink, '_blank');
                            return true;
                        }
                        return false;
                    }.bind(userLabel)
                }, userLabel);
            }

            rowLayer.width = columnWidth;
            rowLayer.height = rowHeight * 2;
            //rowLayer.x = size.width/2;//posX - rowLayer.width / 2 + padding / 2;
            //rowLayer.y = newY - rowHeight * i * 2;
            //this.addChild(rowLayer);
            items.push(rowLayer);
        }
        if (items.length > 0) {
            this.addItemsInRows(items, newY, 10, 10);
        }
        try {
            ga('send', 'event', 'statistic', 'open');
        } catch (e) {
        }

        return this;
    },
    /**
     * align menu items in rows
     * @example
     * // Example
     * menu.alignItemsInRows(5,3)//this will align items to 2 rows, first row with 5 items, second row with 3
     *
     * menu.alignItemsInRows(4,4,4,4)//this creates 4 rows each have 4 items
     * @param items
     * @param startY
     */
    addItemsInRows: function (items, startY/*Multiple arguments*/) {
        if ((arguments.length > 0) && (arguments[arguments.length - 1] == null))
            cc.log("parameters should not be ending with null in Javascript");
        var columns = [], i;
        for (i = 2; i < arguments.length; i++) {
            columns.push(arguments[i]);
        }
        var columnWidths = [];
        var columnHeights = [];

        var winSize = cc.director.getWinSize();
        var width = 0;
        var column = 0;
        var columnWidth = 675 / columns.length - 40 * (columns.length > 1 ? 1 : 0);//winSize.width / 2;
        var columnHeight = items[0].height;
        var rowsOccupied = 0;
        var columnRows, child, len, tmp;

        if (items.length > 0) {
            for (i = 0, len = items.length; i < len; i++) {
                child = items[i];
                // check if too many menu items for the amount of rows/columns
                if (column >= columns.length)
                    continue;

                columnRows = columns[column];
                // can't have zero rows on a column
                if (!columnRows)
                    continue;

                // columnWidth = fmaxf(columnWidth, [item contentSize].width);
                tmp = child.width;
                //columnWidth = ((columnWidth >= tmp || isNaN(tmp)) ? columnWidth : tmp);

                //columnHeight += (child.height + 5);
                ++rowsOccupied;

                if (rowsOccupied >= columnRows) {
                    columnWidths.push(columnWidth);
                    columnHeights.push(columnHeight);
                    width += columnWidth + 10;

                    rowsOccupied = 0;
                    //columnWidth = 0;
                    //columnHeight = -5;
                    ++column;
                }
            }
        }
        // check if too many rows/columns for available menu items.
        //cc.assert(!rowsOccupied, "");

        column = 0;
        //columnWidth = 0;
        columnRows = 0;
        var x = winSize.width - columnWidth / 2;
        var y = 0.0;
        var minY = 0;

        if (items.length > 0) {
            for (i = 0, len = items.length; i < len; i++) {
                child = items[i];
                if (columnRows == 0) {
                    columnRows = columns[column];
                    y = columnHeight;
                }

                // columnWidth = fmaxf(columnWidth, [item contentSize].width);
                tmp = child._getWidth();
                //columnWidth = ((columnWidth >= tmp || isNaN(tmp)) ? columnWidth : tmp);

                var posX = x - (winSize.width - child.width) - 10;//x - child.width / 2 - 10;
                //console.log(column);
                //if (column == 0) {
                //    posX += 20;
                //} else {
                //    posX -= 20;
                //}
                child.setPosition(posX, startY + y - child.height);
                this.addChild(child);
                y -= child.height;// + 10;
                if (child.y < minY) {
                    minY = child.y;
                }
                ++rowsOccupied;

                if (rowsOccupied >= columnRows) {
                    x += columnWidth + 30;
                    rowsOccupied = 0;
                    columnRows = 0;
                    //columnWidth = 0;
                    ++column;
                }
            }
        }
        if (minY < 0) {
            this.height = winSize.height - minY + columnHeight;
        }
    }
});