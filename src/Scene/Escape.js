var SceneEscape;
SceneEscape = SceneGameAbstract.extend({
    timer_start: 420,
    gameLogic: GameEscape,
    init: function () {
        this._class = SceneEscape;
        var size = cc.director.getWinSize();
        this.scoreLayer = ScoreLayer.create({
            fontColor: res.colors.escape
        });
        this.scoreLayer.x = 0;
        this.scoreLayer.y = size.height - this.scoreLayer.height;
        this.addChild(this.scoreLayer);
        this.hud = HudLayer.create({
            timerEnabled: true,
            pauseButton: this.game.pauseButton,
            fontColor: res.colors.escape
        });
        this.hud.x = size.width - this.hud.width;
        this.hud.y = size.height - this.hud.height;
        this.addChild(this.hud);
        cc.eventManager.dispatchCustomEvent(EVENT_TIMER_START, this.timer_start);
        try {
            ga('send', 'event', 'game', 'escape', 'begin');
        } catch (e) {
        }
    },
    showResults: function (win) {
        this.removeAllChildren(true);

        var resultsData;
        //Увеличиваем кол-во сыгранных игр на 1
        var gamesPlayed = cc.UserData['gamesPlayed'] || 0;
        cc.UserData['gamesPlayed'] = gamesPlayed + 1;
        if (win) {
            cc.UserData['escapeStreak']++;
        } else {
            cc.UserData['escapeStreak'] = 0;
        }

        var timer = Timer.getInstance();
        var time = this.timer_start - timer.timerLength;

        resultsData = {
            win: win,
            gamesPlayed: cc.UserData['gamesPlayed'],
            score: this.game.score || 0,
            wordsTotal: Object.size(this.game.foundWords),
            foundWords: this.game.foundWords,
            gameTime: time.toMMSS(),
            gameTimeTitle: t('Time spent'),
            fontColor: res.colors.escape
        };
        this.createResultLayer(resultsData);

        //отправляем статистику
        if (win === true || win === false) {
            var stat = {
                gameType: 'Escape',
                foundWords: this.game.foundWords,
                time: time,
                score: this.game.score,
                win: win,
                tilesCount: this.game.tilesCount,
                tilesRemoved: this.game.tilesRemoved
            };
            cc.eventManager.dispatchCustomEvent(EVENT_SEND_STAT, stat);

            try {
                var finish = win?'win':'lose';
                ga('send', 'event', 'game', 'escape', finish);
            } catch (e) {
            }
        } else {
            try {
                ga('send', 'event', 'game', 'escape', 'finish');
            } catch (e) {
            }
        }
    }
});