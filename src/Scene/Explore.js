var SceneExplore;
SceneExplore = SceneGameAbstract.extend({
    gameLogic: GameExplore,
    endButtonEnabled: true,
    init: function () {
        this._class = SceneExplore;
        var size = cc.director.getWinSize();
        this.scoreLayer = ScoreLayer.create({
            fontColor: res.colors.explore
        });
        this.scoreLayer.x = 0;
        this.scoreLayer.y = size.height - this.scoreLayer.height;
        this.addChild(this.scoreLayer);
        this.hud = HudLayer.create({
            pauseButton: this.game.pauseButton,
            endButtonEnabled: true,
            endButton: this.game.endButton,
            fontColor: res.colors.explore
        });
        this.hud.x = size.width - this.hud.width;
        this.hud.y = size.height - this.hud.height;
        this.addChild(this.hud);
        try {
            ga('send', 'event', 'game', 'explore', 'begin');
        } catch (e) {
        }
    },
    showResults: function (win) {
        this.removeAllChildren(true);

        var resultsData;
        //Увеличиваем кол-во сыгранных игр на 1
        if (Object.size(this.game.foundWords) > 0) {
            var gamesPlayed = cc.UserData['gamesPlayed'] || 0;
            cc.UserData['gamesPlayed'] = gamesPlayed + 1;
            resultsData = {
                //win: win,
                gamesPlayed: cc.UserData['gamesPlayed'],
                score: this.game.score || 0,
                wordsTotal: Object.size(this.game.foundWords),
                foundWords: this.game.foundWords,
                fontColor: res.colors.explore
            };
            //отправляем статистику
            var stat = {
                gameType: 'Explore',
                foundWords: this.game.foundWords,
                score: this.game.score,
                win: win,
                tilesCount: this.game.tilesCount,
                tilesRemoved: this.game.tilesRemoved
            };
            cc.eventManager.dispatchCustomEvent(EVENT_SEND_STAT, stat);

            try {
                ga('send', 'event', 'game', 'explore', 'finish');
            } catch (e) {
            }
        } else {
            resultsData = {};
        }

        this.createResultLayer(resultsData);
    }
});