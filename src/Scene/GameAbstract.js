var SceneGameAbstract;
SceneGameAbstract = cc.Scene.extend({
    gameLogic: null,
    game: null,
    _hexBatch: null,
    _tableLayer: null,
    _class: null,
    wordPreviewLayer: null, //слой для отображения превью слова
    infoBox: null,          //слой для отображения всплывающей информации
    infoRightBox: null,     //блок справа с таймером, очками, паузой
    endButtonEnabled: false,
    listeners: [],
    ctor: function () {
        this._super();
        changeRes($(window).width(), $(window).height());
        cc.spriteFrameCache.addSpriteFrames(res.Game_plist);
        cc.spriteFrameCache.addSpriteFrames(res.Achieve_plist);

        this._tableLayer = new cc.Layer();
        var moved = false;
        this.sceneEventListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,
            onTouchBegan: function (touch, event) {
                return true;
            },
            onTouchMoved: function (touch, event) {
                //Двигаем карту
                if (!TouchBeginTile) {
                    moved = true;
                    var target = event.getCurrentTarget();
                    var delta = touch.getDelta();
                    target.x += delta.x;
                    target.y += delta.y;
                }
                return true;
            },
            onTouchEnded: function (touch, event) {
                cc.eventManager.dispatchCustomEvent(EVENT_END_WORD);
                TouchBeginTile = false;
                if (moved) {
                    cc.eventManager.dispatchCustomEvent(EVENT_TOUCH_MOVED);
                    moved = false;
                }
            }
        });
        this.listeners.push(cc.eventManager.addListener(this.sceneEventListener, this._tableLayer));
        this.listeners.push(cc.eventManager.addCustomListener(EVENT_CENTER_SCENE, function(data){
            var tile = data.getUserData();
            var sceneX = this._tableLayer.width/2 - tile.x;
            var sceneY = this._tableLayer.height/2 - tile.y;
            this._tableLayer.runAction(cc.moveTo(.3, sceneX, sceneY));
        }.bind(this)));

        this.game = this.gameLogic.create();
        var size = cc.director.getWinSize();
        var x0 = size.width / 2 - (3 / 4 * this.game.tileWidth * this.game.size) / 2;
        var y0 = size.height / 2 + this.game.tileHeight * this.game.size / 2;
        var hex = cc.textureCache.addImage(res.Game_png);
        this._hexBatch = new cc.SpriteBatchNode(hex, 50);
        this._tableLayer.addChild(this._hexBatch);
        for (var i = 0; i < this.game.size; i++) {
            for (var j = 0; j < this.game.size; j++) {
                var layer = this.game.table[i][j];
                if (!layer || layer == '*') {
                    continue;
                }
                var height = Math.sqrt(3) / 2 * layer.width - 2;
                var width = layer.width - 2;
                var x = x0 + i * 3 / 4 * width;
                var y = y0 - j * height / 2 - (j + 1) * height / 2 - (i % 2) * height / 2;
                var border = new cc.Sprite('#' + layer.hexagon_border);
                border.x = x;
                border.y = y;
                this._hexBatch.addChild(border);
                layer.attr({
                    x: x,
                    y: y
                });
                this._hexBatch.addChild(layer);
            }
        }
        this.addChild(this._tableLayer);
        this.wordPreviewLayer = WordPreviewLayer.create();
        this.addChild(this.wordPreviewLayer);

        this.infoBox = InfoboxLayer.create();
        this.addChild(this.infoBox);

        this.init();
        this.initEvents();

        return true;
    },
    init: function () {

    },
    initEvents: function () {
        this.listeners.push(cc.eventManager.addCustomListener(EVENT_GAME_PAUSED, function () {
            this.onExit();
            changeRes(winSize.w, winSize.h);
            this.pauseMenu = PauseMenuLayer.create(this.endButtonEnabled);
            this.addChild(this.pauseMenu);
            this.pauseMenu.onEnter();
        }.bind(this)));

        this.listeners.push(cc.eventManager.addCustomListener(EVENT_BUY_HINTS_MENU, function () {
            this.onExit();
            changeRes(winSize.w, winSize.h);
            this.pauseMenu = HintsMenuLayer.create(this.endButtonEnabled);
            this.addChild(this.pauseMenu);
            this.pauseMenu.onEnter();
        }.bind(this)));

        this.listeners.push(cc.eventManager.addCustomListener(EVENT_GAME_RESUMED, function () {
            changeRes($(window).width(), $(window).height());
            this.onEnter();
            if (this.pauseMenu) {
                this.pauseMenu.removeAllChildren(true);
                this.pauseMenu.removeFromParent(true);
            }
        }.bind(this)));

        this.listeners.push(cc.eventManager.addCustomListener(EVENT_NEW_GAME, function () {
            if (this.pauseMenu) {
                this.pauseMenu.removeAllChildren(true);
                this.pauseMenu.removeFromParent(true);
            }
            this.removeAllChildren(true);
            this.removeFromParent(true);
            cc.LoaderScene.preload(getGameResources(), function () {
                Dict = cc.loader.getRes(getDictValue('dict'));
                LETTERS_FREQ = getDictValue('lettersFreq');
                BOMB_LETTERS = getDictValue('bombLetters');
                var scene = new this._class();
                cc.director.runScene(scene);
            }.bind(this), this);
        }.bind(this)));

        this.listeners.push(cc.eventManager.addCustomListener(EVENT_GAME_END, function (data) {
            changeRes(winSize.w, winSize.h);
            var win = data.getUserData();
            this.gameEnd(win);
        }.bind(this)));

        this.listeners.push(cc.eventManager.addCustomListener(EVENT_FLASH_SCORE, function(data) {
            var userData = data.getUserData();
            if (userData.score) {
                var scoreLabel = new cc.LabelTTF('+' + userData.score, 'Intro', 60);
                scoreLabel.setPosition(userData.x, userData.y);
                scoreLabel.setScale(.3);
                this._tableLayer.addChild(scoreLabel);
                scoreLabel.runAction(cc.scaleTo(1, 1, 1));
                scoreLabel.runAction(cc.sequence(
                    cc.fadeOut(1),
                    cc.removeSelf(true)
                ));
            }
        }.bind(this)));
    },
    cleanup: function () {
        this._super();
        this.game.cleanup();
        cc.eventManager.removeListener(this.tileEventListener);
        for (var i in this.listeners) {
            cc.eventManager.removeListener(this.listeners[i]);
        }
    },
    gameEnd: function (win) {
        var size = cc.director.getWinSize();
        var bgLayer = cc.LayerColor.create(cc.color(0, 0, 0, 125), size.width, size.height);
        this.addChild(bgLayer);
        if (this.scoreLabel) {
            this.removeChild(this.scoreLabel, true);
        }
        if (this.hud) {
            this.removeChild(this.hud, true);
        }

        for (var i in this.game.table) {
            for (var j in this.game.table[i]) {
                if (typeof this.game.table[i][j] == 'object') {
                    this.game.table[i][j].removeSprite();
                }
            }
        }

        if (win !== undefined) {
            this.showResults(win);
        } else {
            this.showResults();
        }
    },
    showResults: function (win) {
        console.log('Override');
    },

    createResultLayer: function (data) {
        var resultsLayer = ResultsLayer.create(data);
        var backgroundScene = new BackgroundScene(resultsLayer);
        this.addChild(backgroundScene);
    }
});