var SceneSurvive;
SceneSurvive = SceneGameAbstract.extend({
    gameLogic: GameSurvive,
    timer_start: 30,
    init: function () {
        this._class = SceneSurvive;
        var size = cc.director.getWinSize();
        this.hud = HudLayer.create({
            timerEnabled: true,
            pauseButton: this.game.pauseButton,
            fontColor: res.colors.survive
        });
        this.hud.x = size.width - this.hud.width;
        this.hud.y = size.height - this.hud.height;
        this.addChild(this.hud);
        cc.eventManager.dispatchCustomEvent(EVENT_TIMER_START, this.timer_start);
        try {
            ga('send', 'event', 'game', 'survive', 'begin');
        } catch (e) {
        }
    },
    showResults: function (win) {
        this.removeAllChildren(true);

        var resultsData;
        //Увеличиваем кол-во сыгранных игр на 1
        var gamesPlayed = cc.UserData['gamesPlayed'] || 0;
        cc.UserData['gamesPlayed'] = gamesPlayed + 1;

        resultsData = {
            win: win,
            gamesPlayed: cc.UserData['gamesPlayed'],
            score: this.game.score || 0,
            wordsTotal: Object.size(this.game.foundWords),
            foundWords: this.game.foundWords,
            gameTime: this.game.surviveTime,
            gameTimeTitle: t('Time spent, sec') + ":",
            fontColor: res.colors.survive
        };
        this.createResultLayer(resultsData);

        //отправляем статистику
        if (win != undefined) {
            var stat = {
                gameType: 'Survive',
                foundWords: this.game.foundWords,
                time: this.game.surviveTime,
                score: this.game.score,
                tilesCount: this.game.tilesCount,
                tilesRemoved: this.game.tilesRemoved
            };
            cc.eventManager.dispatchCustomEvent(EVENT_SEND_STAT, stat);
            try {
                ga('send', 'event', 'game', 'survive', 'finish');
            } catch (e) {
            }
        }
    }
});