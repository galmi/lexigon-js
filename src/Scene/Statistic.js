var StatisticScene = cc.Scene.extend({
    ctor: function (data, statType) {
        this._super();
        changeRes(winSize.w, winSize.h);
        var size = cc.director.getWinSize();
        var i, index, rowLayer, numLabel, scoreLabel, userLabel;
        var padding = 20;

        var background = new cc.Sprite(res.Background_png);
        background.x = size.width / 2;
        background.y = size.height / 2;
        this.addChild(background);

        var titleLabel = new cc.LabelTTF(statType, "Intro", 50);
        titleLabel.setColor(cc.color(213, 213, 213, 255));
        titleLabel.setPositionX(size.width / 2);
        titleLabel.setPositionY(size.height - titleLabel.height / 2 - padding);
        this.addChild(titleLabel);

        var mainMenuButton = new cc.MenuItemSprite(
            new cc.Sprite('#' + res.sprite.menuNormal),
            new cc.Sprite('#' + res.sprite.menuSelected),
            null,
            function () {
                var scene = MainMenu.scene();
                cc.director.runScene(scene);
            }
        );
        var mainMenuText = new cc.LabelBMFont(t('Main menu'), res.FntIntro25Gray);
        mainMenuText.x = mainMenuButton.width / 2;
        mainMenuText.y = mainMenuButton.height / 2;
        mainMenuButton.addChild(mainMenuText);
        var menu = new cc.Menu([mainMenuButton]);
        menu.width = mainMenuButton.width;
        menu.height = mainMenuButton.height;
        menu.setPositionY(titleLabel.y - titleLabel.height / 2 - padding * 2);
        this.addChild(menu);

        var newY = menu.y - menu.height / 2 - padding;

        var lineHoriz = new cc.Sprite('#' + 'line_horiz.png');
        lineHoriz.x = size.width / 2;
        lineHoriz.y = newY;
        this.addChild(lineHoriz);

        var surviveLabel = new cc.LabelTTF(t('Survive'), "Intro", 25);
        surviveLabel.setFontFillColor(res.colors.survive);
        surviveLabel.setPositionX(size.width / 5);
        surviveLabel.setPositionY(newY - surviveLabel.height / 2 - padding);
        this.addChild(surviveLabel);

        var escapeLabel = new cc.LabelTTF(t('Escape'), "Intro", 25);
        escapeLabel.setFontFillColor(res.colors.escape);
        escapeLabel.setPositionX(size.width / 2);
        escapeLabel.setPositionY(newY - escapeLabel.height / 2 - padding);
        this.addChild(escapeLabel);

        var exploreLabel = new cc.LabelTTF(t('Explore'), "Intro", 25);
        exploreLabel.setFontFillColor(res.colors.explore);
        exploreLabel.setPositionX(size.width * 4 / 5);
        exploreLabel.setPositionY(newY - exploreLabel.height / 2 - padding);
        this.addChild(exploreLabel);

        newY = exploreLabel.y - exploreLabel.height / 2 - padding / 2;

        var lineHoriz2 = new cc.Sprite('#' + 'line_horiz.png');
        lineHoriz2.x = size.width / 2;
        lineHoriz2.y = newY;
        this.addChild(lineHoriz2);

        var lineVertical = new cc.Sprite('#' + 'line_vertical_normal.png');
        lineVertical.x = lineHoriz2.width / 3 + lineHoriz2.x - lineHoriz2.width / 2 - padding / 4;
        lineVertical.y = lineHoriz2.y - lineVertical.height / 2 - padding / 2;
        this.addChild(lineVertical);

        var lineVertical2 = new cc.Sprite('#' + 'line_vertical_normal.png');
        lineVertical2.x = lineHoriz2.width * 2 / 3 + lineHoriz2.x - lineHoriz2.width / 2 + padding / 4;
        lineVertical2.y = lineHoriz2.y - lineVertical.height / 2 - padding / 2;
        this.addChild(lineVertical2);

        newY = lineHoriz2.y - padding;
        var columnWidth = lineHoriz.width / 3;
        var rowHeight = 23;
        if (data.hasOwnProperty('Survive') && data.Survive.length > 0) {
            var surviveX = lineHoriz.x - lineHoriz.width / 2;
            for (i in data.Survive) {
                index = parseInt(i) + 1;
                rowLayer = new cc.Layer();
                rowLayer.width = columnWidth;
                numLabel = new cc.LabelTTF(index + '.', "Intro", 18, cc.size(columnWidth, rowHeight), cc.TEXT_ALIGNMENT_LEFT);
                numLabel.setFontFillColor(cc.color(153, 153, 153, 255));
                numLabel.setPosition(0, 0);
                rowLayer.addChild(numLabel);
                scoreLabel = new cc.LabelTTF(data.Survive[i].value + 'сек.', "Intro", 18, cc.size(columnWidth, rowHeight), cc.TEXT_ALIGNMENT_RIGHT);
                scoreLabel.setFontFillColor(cc.color(213, 213, 213, 255));
                scoreLabel.setPosition(0, 0);
                rowLayer.addChild(scoreLabel);
                userLabel = new cc.LabelTTF(data.Survive[i].first_name + "\n" + data.Survive[i].last_name, "Arial", 18, cc.size(columnWidth / 2, rowHeight * 2), cc.TEXT_ALIGNMENT_LEFT);
                userLabel.setFontFillColor(res.colors.survive);
                userLabel.setPosition(25 - userLabel.width / 2, -rowHeight / 2);
                rowLayer.addChild(userLabel);
                if (data.Survive[i].link) {
                    userLabel.userLink = data.Survive[i].link;
                    cc.eventManager.addListener({
                        event: cc.EventListener.TOUCH_ONE_BY_ONE,
                        onTouchBegan: function (touch, event) {
                            var target = event.getCurrentTarget();
                            var locationInNode = target.convertToNodeSpace(touch.getLocation());
                            var s = target.getContentSize();
                            var rect = cc.rect(0, 0, s.width, s.height);

                            if (cc.rectContainsPoint(rect, locationInNode)) {
                                window.open(this.userLink, '_blank');
                                return true;
                            }
                            return false;
                        }.bind(userLabel)
                    }, userLabel);
                }
                rowLayer.width = columnWidth;
                rowLayer.height = Math.max(numLabel.height, userLabel.height, scoreLabel.height);
                rowLayer.x = surviveX + columnWidth / 2 - padding / 2;
                rowLayer.y = newY - rowHeight * i * 2;
                this.addChild(rowLayer);
            }
        }

        if (data.hasOwnProperty('Escape') && data.Escape.length > 0) {
            var escapeX = lineHoriz.x;
            for (i in data.Escape) {
                index = parseInt(i) + 1;
                rowLayer = new cc.Layer();
                rowLayer.width = columnWidth;
                numLabel = new cc.LabelTTF(index + '.', "Intro", 18, cc.size(columnWidth, rowHeight), cc.TEXT_ALIGNMENT_LEFT);
                numLabel.setFontFillColor(cc.color(153, 153, 153, 255));
                numLabel.setPosition(0, 0);
                rowLayer.addChild(numLabel);
                scoreLabel = new cc.LabelTTF(parseInt(data.Escape[i].value).toMMSS(), "Intro", 18, cc.size(columnWidth, rowHeight), cc.TEXT_ALIGNMENT_RIGHT);
                scoreLabel.setFontFillColor(cc.color(213, 213, 213, 255));
                scoreLabel.setPosition(0, 0);
                rowLayer.addChild(scoreLabel);
                userLabel = new cc.LabelTTF(data.Escape[i].first_name + "\n" + data.Escape[i].last_name, "Arial", 18, cc.size(columnWidth / 2, rowHeight * 2), cc.TEXT_ALIGNMENT_LEFT);
                userLabel.setFontFillColor(res.colors.escape);
                userLabel.setPosition(25 - userLabel.width / 2, 2 - rowHeight / 2);
                rowLayer.addChild(userLabel);
                if (data.Escape[i].link) {
                    userLabel.userLink = data.Escape[i].link;
                    cc.eventManager.addListener({
                        event: cc.EventListener.TOUCH_ONE_BY_ONE,
                        onTouchBegan: function (touch, event) {
                            var target = event.getCurrentTarget();
                            var locationInNode = target.convertToNodeSpace(touch.getLocation());
                            var s = target.getContentSize();
                            var rect = cc.rect(0, 0, s.width, s.height);

                            if (cc.rectContainsPoint(rect, locationInNode)) {
                                window.open(this.userLink, '_blank');
                                return true;
                            }
                            return false;
                        }.bind(userLabel)
                    }, userLabel);
                }

                rowLayer.width = columnWidth;
                rowLayer.height = Math.max(numLabel.height, userLabel.height, scoreLabel.height);
                rowLayer.x = escapeX;
                rowLayer.y = newY - rowHeight * i * 2;
                this.addChild(rowLayer);
            }
        }

        if (data.hasOwnProperty('Explore') && data.Explore.length > 0) {
            var exploreX = lineHoriz.x + lineHoriz.width / 2;
            for (i in data.Explore) {
                index = parseInt(i) + 1;
                rowLayer = new cc.Layer();
                rowLayer.width = columnWidth;
                numLabel = new cc.LabelTTF(index + '.', "Intro", 18, cc.size(columnWidth, rowHeight), cc.TEXT_ALIGNMENT_LEFT);
                numLabel.setFontFillColor(cc.color(153, 153, 153, 255));
                numLabel.setPosition(0, 0);
                rowLayer.addChild(numLabel);
                scoreLabel = new cc.LabelTTF(data.Explore[i].value, "Intro", 18, cc.size(columnWidth, rowHeight), cc.TEXT_ALIGNMENT_RIGHT);
                scoreLabel.setFontFillColor(cc.color(213, 213, 213, 255));
                scoreLabel.setPosition(0, 0);
                rowLayer.addChild(scoreLabel);
                userLabel = new cc.LabelTTF(data.Explore[i].first_name + "\n" + data.Explore[i].last_name, "Arial", 18, cc.size(columnWidth / 2, rowHeight * 2), cc.TEXT_ALIGNMENT_LEFT);
                userLabel.setFontFillColor(res.colors.explore);
                userLabel.setPosition(25 - userLabel.width / 2, 2 - rowHeight / 2);
                rowLayer.addChild(userLabel);
                if (data.Explore[i].link) {
                    userLabel.userLink = data.Explore[i].link;
                    cc.eventManager.addListener({
                        event: cc.EventListener.TOUCH_ONE_BY_ONE,
                        onTouchBegan: function (touch, event) {
                            var target = event.getCurrentTarget();
                            var locationInNode = target.convertToNodeSpace(touch.getLocation());
                            var s = target.getContentSize();
                            var rect = cc.rect(0, 0, s.width, s.height);

                            if (cc.rectContainsPoint(rect, locationInNode)) {
                                window.open(this.userLink, '_blank');
                                return true;
                            }
                            return false;
                        }.bind(userLabel)
                    }, userLabel);
                }

                rowLayer.width = columnWidth;
                rowLayer.height = rowHeight * 2;
                rowLayer.x = exploreX - rowLayer.width / 2 + padding / 2;
                rowLayer.y = newY - rowHeight * i * 2;
                this.addChild(rowLayer);
            }
        }

        try {
            ga('send', 'event', 'statistic', 'open');
        } catch (e) {
        }

        return this;
    },
    getUserSprite: function (index, data, value) {
        var padding = 10;
        var layer = new cc.Layer();
        var sprite = new cc.Sprite(CDI(data.photo));
        sprite.width = 50;
        sprite.height = 50;
        var indexLabel = new cc.LabelTTF(index, "Arial", 18);
        var textLabel = new cc.LabelTTF(data.first_name + ' ' + data.last_name + "\n" + value, "Arial", 18);
        var width = indexLabel.width + sprite.width + textLabel.width + padding * 2;
        var height = sprite.height;

        sprite.x = -width / 2 + (sprite.width + indexLabel.width);
        sprite.y = 0;
        indexLabel.x = sprite.x - sprite.width / 2 - indexLabel.width - padding;
        indexLabel.y = sprite.y;
        textLabel.x = sprite.x + sprite.width / 2 + textLabel.width / 2 + padding;
        textLabel.y = sprite.y;
        layer.addChild(indexLabel);
        layer.addChild(sprite);
        layer.addChild(textLabel);
        layer.width = width;
        layer.height = height;
        return layer;
    }
});