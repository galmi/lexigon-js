var AchievementsScene = cc.Scene.extend({
    ctor: function () {
        this._super();
        var size = cc.director.getWinSize();
        var i, index, rowLayer, numLabel, scoreLabel, userLabel;
        var padding = 20;

        var titleLabel = new cc.LabelTTF(t('Achievements'), "Intro", 50);
        titleLabel.setColor(cc.color(213, 213, 213, 255));
        titleLabel.setPositionX(size.width / 2);
        titleLabel.setPositionY(size.height - titleLabel.height / 2 - padding);
        this.addChild(titleLabel);

        var menuItemSprite = new cc.Sprite('#' + res.sprite.menuNormal);
        var mainMenuButton = new cc.MenuItemSprite(
            menuItemSprite,
            new cc.Sprite('#' + res.sprite.menuSelected),
            null,
            function () {
                var scene = MainMenu.scene();
                cc.director.runScene(scene);
            }
        );
        var mainMenuText = new cc.LabelBMFont(t('Main menu'), res.FntIntro25Gray);
        mainMenuText.x = menuItemSprite.width / 2;
        mainMenuText.y = menuItemSprite.height / 2;
        mainMenuButton.addChild(mainMenuText);
        var menu = new cc.Menu([mainMenuButton]);
        menu.width = mainMenuButton.width;
        menu.height = mainMenuButton.height;
        menu.setPositionY(titleLabel.y - titleLabel.height / 2 - padding * 2);
        this.addChild(menu);

        var newY = menu.y - menu.height / 2 - padding;

        var lineHoriz = new cc.Sprite('#' + 'line_horiz.png');
        lineHoriz.x = size.width / 2;
        lineHoriz.y = newY;
        this.addChild(lineHoriz);

        var columnWidth = lineHoriz.width / 2;
        var rowHeight = 41;
        newY = lineHoriz.y - padding - rowHeight - padding;

        var posX = lineHoriz.x;// - lineHoriz.width / 2;
        var items = [];
        var achievePoints = 0;
        for (i in achievements) {
            index = parseInt(i) + 1;
            var achieve = achievements[i];

            rowLayer = new cc.Node();
            rowLayer.width = lineHoriz.width;
            rowLayer.height = rowHeight;

            var iconSprite;
            if (cc.UserData['achievements'].indexOf(achieve.id) > -1) {
                iconSprite = new cc.Sprite('#' + achieve.icon);
                achievePoints += achieve.pts;
            } else {
                iconSprite = new cc.Sprite('#achievement_new.png');
            }
            iconSprite.setPosition(iconSprite.width / 2 + 10, rowLayer.height / 2);
            rowLayer.addChild(iconSprite);
            var label = new cc.LabelBMFont(achieve.name, res.FntIntro20White);
            label.setColor(cc.color(39, 170, 225));
            label.setPositionX(iconSprite.x + iconSprite.width / 2 + label.width / 2 + 10);
            label.setPositionY(rowLayer.height / 2 + label.height);
            rowLayer.addChild(label);

            var description = new cc.LabelTTF(achieve.description, 'Arial', 14, cc.size(rowLayer.width - iconSprite.width - 10));
            description.setColor(cc.color(213, 213, 213, 255));
            description.setPositionX(iconSprite.x + iconSprite.width / 2 + description.width / 2 + 10);
            description.setPositionY(label.y - description.height / 2 - 10);
            rowLayer.addChild(description);

            var pts = new cc.LabelTTF(achieve.pts + 'pts', 'Arial', 20);
            pts.setPosition(columnWidth - pts.width, 0);
            rowLayer.addChild(pts);

            rowLayer.width = columnWidth;
            rowLayer.height = rowHeight * 2;
            items.push(rowLayer);
        }
        var achievePtsLabel = new cc.LabelTTF(t('Your score') + ': ' + achievePoints, 'Arial', 20);
        achievePtsLabel.setPosition(size.width / 2, newY + achievePtsLabel.height + padding);
        this.addChild(achievePtsLabel);

        newY -= achievePtsLabel.height + padding;

        if (items.length > 0) {
            this.addItemsInRows(items, newY, Math.floor(items.length / 2), Math.ceil(items.length / 2));
        }
        this.mouseEventListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            onMouseScroll: function (event) {
                var size = cc.director.getWinSize();
                var target = event.getCurrentTarget();

                if (target.height <= size.height) {
                    return;
                }

                var currentY = target.y;
                var newY = currentY - event.getScrollY();
                var maxY = 0;//size.height - target.height / 2;
                var minY = (target.height - size.height);
                if (newY < maxY) {
                    newY = maxY;
                } else if (newY > minY) {
                    newY = minY;
                }
                target.y = newY;
            }.bind(this)
        });
        cc.eventManager.addListener(this.mouseEventListener, this);

        try {
            ga('send', 'event', 'achievements', 'open');
        } catch (e) {
        }

        return this;
    },
    /**
     * align menu items in rows
     * @example
     * // Example
     * menu.alignItemsInRows(5,3)//this will align items to 2 rows, first row with 5 items, second row with 3
     *
     * menu.alignItemsInRows(4,4,4,4)//this creates 4 rows each have 4 items
     * @param items
     * @param startY
     */
    addItemsInRows: function (items, startY/*Multiple arguments*/) {
        if ((arguments.length > 0) && (arguments[arguments.length - 1] == null))
            cc.log("parameters should not be ending with null in Javascript");
        var columns = [], i;
        for (i = 2; i < arguments.length; i++) {
            columns.push(arguments[i]);
        }
        var columnWidths = [];
        var columnHeights = [];

        var winSize = cc.director.getWinSize();
        var width = 0;
        var column = 0;
        var columnWidth = 675 / columns.length - 40 * (columns.length > 1 ? 1 : 0);//winSize.width / 2;
        var columnHeight = items[0].height;
        var rowsOccupied = 0;
        var columnRows, child, len, tmp;

        if (items.length > 0) {
            for (i = 0, len = items.length; i < len; i++) {
                child = items[i];
                // check if too many menu items for the amount of rows/columns
                if (column >= columns.length)
                    continue;

                columnRows = columns[column];
                // can't have zero rows on a column
                if (!columnRows)
                    continue;

                ++rowsOccupied;

                if (rowsOccupied >= columnRows) {
                    columnWidths.push(columnWidth);
                    columnHeights.push(columnHeight);
                    width += columnWidth + 10;

                    rowsOccupied = 0;
                    ++column;
                }
            }
        }

        column = 0;
        columnRows = 0;
        var x = winSize.width / 2;
        var y = 0.0;
        var minY = 0;

        if (items.length > 0) {
            for (i = 0, len = items.length; i < len; i++) {
                child = items[i];
                if (columnRows == 0) {
                    columnRows = columns[column];
                    y = columnHeight;
                }

                var posX = x - ( child.width);//x - child.width / 2 - 10;
                child.setPosition(posX, startY + y - child.height);
                this.addChild(child);
                y -= child.height;// + 10;
                if (child.y < minY) {
                    minY = child.y;
                }
                ++rowsOccupied;

                if (i % 2 == 0) {
                    x += columnWidth + 40;
                    y += child.height;
                    ++column;
                } else {
                    x -= columnWidth + 40;
                    columnRows--;
                    --column;
                }
            }
        }
        if (minY < 0) {
            this.height = winSize.height - minY + columnHeight;
        }
    }
});