var ScoreLayer = cc.LayerColor.extend({
    bgColor: cc.color(50, 66, 69, 128),
    paddingWidth: 20,
    paddingHeight: 0,
    fontColor: cc.color(255, 255, 255, 255),
    listeners: [],
    init: function(params) {
        for (var i in params) {
            this[i] = params[i];
        }
        this.setColor(cc.color(50, 66, 69, 255));
        this.setOpacity(150);
        this.scoreLabel = ScoreLabel.create(0);
        this.addChild(this.scoreLabel);
        var height = this.scoreLabel.height + this.paddingHeight;
        this.setContentSize(this.calcWidth(), height);
        this.updatePositions();
        this.scoreLabel.addLoadedEventListener(function() {
            var height = this.scoreLabel.height + this.paddingHeight;
            this.setContentSize(this.calcWidth(), height);
            this.updatePositions();
        }, this);
        this.listeners.push(cc.eventManager.addCustomListener(EVENT_UPDATED_SCORE, function () {
            var oldWidth = this.width;
            var newWidth = this.calcWidth();
            this.changeWidth(newWidth);
            this.setPositionX(this.x - (newWidth - oldWidth));
            this.updatePositions();
        }.bind(this)));

        return true;
    },

    updatePositions: function () {
        var size = cc.director.getWinSize();
        this.x = 0;//this.padding + this.scoreLabel.width / 2;
        this.y = size.height - this.height;//(this.height - this.scoreLabel.height) / 2;

        this.scoreLabel.x = this.width / 2;
        this.scoreLabel.y = this.height / 2;
    },

    calcWidth: function () {
        var width = this.scoreLabel.width;
        return width + this.paddingWidth;
    },

    cleanup: function() {
        for (var i in this.listeners) {
            cc.eventManager.removeListener(this.listeners[i]);
        }
    }
});

ScoreLayer.create = function (params) {
    var obj = new ScoreLayer();
    if (obj && obj.init(params)) {
        return obj;
    }
    return null;
};