var ScoreLabel = cc.LabelBMFont.extend({
    prefix: '',
    listeners: [],
    init: function () {
        this.setScore(0);
        this.listeners.push(cc.eventManager.addCustomListener(EVENT_UPDATED_SCORE, function (data) {
            var addedScore = data.getUserData();
            this.setScore(addedScore);
        }.bind(this)));
        return true;
    },
    setScore: function (score) {
        if (this.prefix.length > 0) {
            score = this.prefix + score;
        }
        this.setString(score);
    },
    cleanup: function() {
        this._super();
        for (var i in this.listeners) {
            cc.eventManager.removeListener(this.listeners[i]);
        }
    }
});

ScoreLabel.create = function () {
    var obj = new ScoreLabel('0', res.FntIntro40White);
    if (obj && obj.init()) {
        return obj;
    }
    return null;
};