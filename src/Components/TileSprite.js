var TileSprite = cc.Sprite.extend({
    hexagon_normal:     null,
    hexagon_selected:   null,
    hexagon_white:      "hexagon_white.png",
    hexagon_bomb:       null,
    hexagon_bomb_selected: "hexagon_bomb_selected.png",
    hexagon_border:     "hexagon_border.png",
    init: function (x, y, letter, params) {
        if (this._super()) {
            for (var i in params) {
                this[i] = params[i];
            }
            this.isBomb = this.isBombLetter(letter);
            this.tableX = x;
            this.tableY = y;
            this.letter = letter;
            var spriteFrame = cc.spriteFrameCache.getSpriteFrame(this.hexagon_normal);
            this.setSpriteFrame(spriteFrame);

            var letterPng = letter + ".png";
            this.label = new cc.Sprite();
            this.label.setSpriteFrame(letterPng);
            this.label.x = this.width / 2;
            this.label.y = this.height / 2;
            this.addChild(this.label);

            if (this.isBomb) {
                var bombSpriteFrame = cc.spriteFrameCache.getSpriteFrame(this.hexagon_bomb);
                this.bombSprite = new cc.Sprite();
                this.bombSprite.initWithSpriteFrame(bombSpriteFrame);
                this.bombSprite.setOpacity(200);
                this.bombSprite.x = this.width / 2;
                this.bombSprite.y = this.height / 2;
                this.addChild(this.bombSprite);
            }
            this.addEvents();
            return true;
        }
        return false;
    },
    selectSprite: function () {
        this.setSpriteFrame(cc.spriteFrameCache.getSpriteFrame(this.hexagon_selected));
        if (this.isBomb) {
            this.bombSprite.setSpriteFrame(cc.spriteFrameCache.getSpriteFrame(this.hexagon_bomb_selected));
        }
    },
    resetSprite: function () {
        this.setSpriteFrame(cc.spriteFrameCache.getSpriteFrame(this.hexagon_normal));
        if (this.isBomb) {
            this.bombSprite.setSpriteFrame(cc.spriteFrameCache.getSpriteFrame(this.hexagon_bomb));
        }
    },
    removeSprite: function () {
        var cmp = this;
        cc.eventManager.removeListener(this.tileEventListener);
        var fadeOut = cc.fadeOut(.3);
        this.runAction(cc.sequence(fadeOut,
                cc.callFunc(function () {
                    cc.eventManager.dispatchCustomEvent(EVENT_TILE_REMOVE, {x: cmp.tableX, y: cmp.tableY});
                }, this, null)
            )
        );
    },
    hasWordSprite: function () {
        this.setSpriteFrame(cc.spriteFrameCache.getSpriteFrame(this.hexagon_white));
        if (this.isBomb) {
            this.bombSprite.setSpriteFrame(cc.spriteFrameCache.getSpriteFrame(this.hexagon_bomb));
        }
    },

    isBombLetter: function (letter) {
        return BOMB_LETTERS.indexOf(letter) >= 0;
    },
    addEvents: function () {
        var cmp = this;
        this.tileEventListener = cc.EventListener.create({
            event: cc.EventListener.TOUCH_ONE_BY_ONE,

            onTouchBegan: function (touch, event) {
                var target = event.getCurrentTarget();
                var locationInNode = target.convertToNodeSpace(touch.getLocation());
                var s = target.getContentSize();
                var rect = cc.rect(0, 0, s.width, s.height);
                // если клик был внутри шестигранника, значит будем набирать буквы
                if (cc.hexagonContainsPoint(rect, locationInNode)) {
                    TouchBeginTile = true;
                    cc.eventManager.dispatchCustomEvent(EVENT_NEW_WORD, cmp);
                }
                return true;
            },
            onTouchMoved: function (touch, event) {
                if (TouchBeginTile) {
                    var target = event.getCurrentTarget();
                    var locationInNode = target.convertToNodeSpace(touch.getLocation());
                    var s = target.getContentSize();
                    var rect = cc.rect(0, 0, s.width, s.height);
                    //если первый клик был внутри гексагона, набираем слово из букв
                    if (cc.hexagonContainsPoint(rect, locationInNode)) {
                        cc.eventManager.dispatchCustomEvent(EVENT_ADD_LETTER, cmp);
                    }
                }
                return true;
            }
        });
        cc.eventManager.addListener(this.tileEventListener, this);
    }
});

TileSprite.create = function (x, y, letter, params) {
    var obj = new TileSprite();
    if (obj && obj.init(x, y, letter, params)) {
        return obj;
    }
    return null;
};