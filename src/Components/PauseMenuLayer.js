var PauseMenuLayer = cc.LayerColor.extend({
    endButtonEnabled: false,
    init: function () {
        var fontSize = 25;
        var fontColor = cc.color(50, 65, 68, 255);
        var menuItems = [];
        var size = cc.director.getWinSize();
        this.setColor(cc.color(0, 0, 0, 200));
        this.setContentSize(size.width, size.height);

        var background = new cc.Sprite(res.Background_png);
        background.x = size.width / 2;
        background.y = size.height / 2;
        this.addChild(background);

        var title = new cc.LabelTTF(t('Lexigon'), 'Intro', 90);
        title.setFontFillColor(cc.color(214,213,213));
        title.x = size.width / 2;
        title.y = size.height - title.height/2 - 20;
        this.addChild(title);

        if (this.endButtonEnabled) {
            var endButton = new cc.MenuItemSprite(
                new cc.Sprite('#' + res.sprite.menuNormal),
                new cc.Sprite('#' + res.sprite.menuSelected),
                null,
                function () {
                    cc.eventManager.dispatchCustomEvent(EVENT_GAME_RESUMED);
                    cc.eventManager.dispatchCustomEvent(EVENT_GAME_END);
                }
            );
            var endText = new cc.LabelBMFont(t('End game'), res.FntIntro25Gray);
            endText.x = endButton.width / 2;
            endText.y = endButton.height / 2;
            endButton.addChild(endText);
            menuItems.push(endButton);

            var blankButton = new cc.MenuItemSprite(
                new cc.Sprite('#' + res.sprite.menuBlank),
                new cc.Sprite('#' + res.sprite.menuBlank),
                null,
                function () {
                }
            );
            menuItems.push(blankButton);
        }
        var resumeButton = new cc.MenuItemSprite(
            new cc.Sprite('#' + res.sprite.menuNormal),
            new cc.Sprite('#' + res.sprite.menuSelected),
            null,
            function () {
                cc.eventManager.dispatchCustomEvent(EVENT_GAME_RESUMED);
            }
        );
        var resumeText = new cc.LabelBMFont(t('Continue'), res.FntIntro25Gray);
        resumeText.x = resumeButton.width / 2;
        resumeText.y = resumeButton.height / 2;
        resumeButton.addChild(resumeText);
        menuItems.push(resumeButton);

        var mainMenuButton = new cc.MenuItemSprite(
            new cc.Sprite('#' + res.sprite.menuNormal),
            new cc.Sprite('#' + res.sprite.menuSelected),
            null,
            function () {
                cc.LoaderScene.preload(g_menu, function () {
                    var scene = MainMenu.scene();
                    cc.director.runScene(scene);
                }, this);
            }
        );
        var mainMenuText = new cc.LabelBMFont(t('Main menu'), res.FntIntro25Gray);
        mainMenuText.x = mainMenuButton.width / 2;
        mainMenuText.y = mainMenuButton.height / 2;
        mainMenuButton.addChild(mainMenuText);
        menuItems.push(mainMenuButton);

        var newGameButton = new cc.MenuItemSprite(
            new cc.Sprite('#' + res.sprite.menuNormal),
            new cc.Sprite('#' + res.sprite.menuSelected),
            null,
            function () {
                this.removeAllChildren(true);
                this.removeFromParent(true);
                cc.eventManager.dispatchCustomEvent(EVENT_NEW_GAME);
            }.bind(this)
        );
        var newGameText = new cc.LabelBMFont(t('New game'), res.FntIntro25Gray);
        newGameText.x = newGameButton.width / 2;
        newGameText.y = newGameButton.height / 2;
        newGameButton.addChild(newGameText);
        menuItems.push(newGameButton);

        var menu = new cc.Menu(menuItems);
        menu.alignItemsVerticallyWithPadding(10);
        menu.x = size.width / 2;
        menu.y = size.height / 2;
        this.addChild(menu);

        return true;
    }
});

PauseMenuLayer.create = function (endButtonEnabled) {
    var obj = new PauseMenuLayer();
    obj.endButtonEnabled = endButtonEnabled;
    if (obj && obj.init()) {
        return obj;
    }
    return null;
};