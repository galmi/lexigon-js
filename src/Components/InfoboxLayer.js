var InfoboxLayer = cc.LayerColor.extend({
    listeners: [],
    init: function () {
        var created = false;
        if (this._super()) {
            var size = cc.director.getWinSize();
            var layerHeight = 60;
            this.setColor(cc.color(0, 0, 0, 255));
            this.setOpacity(200);
            this.setContentSize(size.width, layerHeight);
            this.setZOrder(1000);
            this.setPositionY(size.height + this.height);
            this.listeners.push(cc.eventManager.addCustomListener(EVENT_SHOW_INFO, function (data) {
                var message = data.getUserData();
                var deltaX = 14;
                var deltaY = 20;
                var size = cc.director.getWinSize();
                var label = new cc.LabelTTF(message, "Intro", 25, cc.size(size.width, layerHeight), cc.TEXT_ALIGNMENT_CENTER, cc.VERTICAL_TEXT_ALIGNMENT_CENTER);
                this.setContentSize(label.width + deltaX, label.height + deltaY);
                this.setPositionX(0);
                this.setPositionY(size.height - 1);
                label.setPositionX(label.width / 2 + deltaX / 2);
                label.setPositionY(label.height / 2 + deltaY / 2);
                this.addChild(label);

                var show = new cc.MoveTo(.5, cc.p(0, size.height - this.height));
                var pause = cc.actionInterval(2);
                pause.update = function () {
                };
                var hide = new cc.MoveTo(.5, cc.p(0, size.height + this.height));
                var func = new cc.CallFunc(this.removeFromParent, label);
                this.runAction(cc.sequence(show, pause, hide, func));
            }.bind(this)));
            created = true;
        }

        return created;
    },
    update: function () {
        return true;
    },
    cleanup: function() {
        this._super();
        for (var i in this.listeners) {
            cc.eventManager.removeListener(this.listeners[i]);
        }
    }
});

InfoboxLayer.create = function () {
    var obj = new InfoboxLayer();
    if (obj && obj.init()) {
        return obj;
    }
    return null;
};