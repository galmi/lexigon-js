var DictionaryMenuLayer = cc.LayerColor.extend({
    init: function () {
        var fontSize = 25;
        var fontColor = cc.color(50, 65, 68, 255);
        var menuItems = [];
        var size = cc.director.getWinSize();
        this.setColor(cc.color(0, 0, 0, 200));
        this.setContentSize(size.width, size.height);

        var background = new cc.Sprite(res.Background_png);
        background.x = size.width / 2;
        background.y = size.height / 2;
        this.addChild(background);

        var title = new cc.LabelTTF(t('Choose dictionary'), 'Intro', 50);
        title.setFontFillColor(cc.color(214,213,213));
        title.x = size.width / 2;
        title.y = size.height - title.height/2 - 20;
        this.addChild(title);

        var ruButton = new cc.MenuItemSprite(
            new cc.Sprite('#' + res.sprite.menuNormal),
            new cc.Sprite('#' + res.sprite.menuSelected),
            null,
            function () {
                setDict('ru');
                try {
                    ga('send', 'event', 'game', 'changed_dict', 'ru');
                } catch (e) {
                }
                var scene = MainMenu.scene();
                cc.director.runScene(scene);
            }
        );
        var ruText = new cc.LabelBMFont(t('Russian'), res.FntIntro25Gray);
        ruText.x = ruButton.width / 2;
        ruText.y = ruButton.height / 2;
        ruButton.addChild(ruText);
        menuItems.push(ruButton);

        var enButtonSprite = new cc.Sprite('#' + res.sprite.menuNormal);
        var enButton = new cc.MenuItemSprite(
            enButtonSprite,
            new cc.Sprite('#' + res.sprite.menuSelected),
            null,
            function () {
                setDict('en');
                try {
                    ga('send', 'event', 'game', 'changed_dict', 'en');
                } catch (e) {
                }
                var scene = MainMenu.scene();
                cc.director.runScene(scene);
            }
        );
        var enText = new cc.LabelBMFont(t('English'), res.FntIntro25Gray);
        enText.x = enButtonSprite.width / 2;
        enText.y = enButtonSprite.height / 2;
        enButton.addChild(enText);
        menuItems.push(enButton);

        var menu = new cc.Menu(menuItems);
        menu.alignItemsVerticallyWithPadding(10);
        menu.x = size.width / 2;
        menu.y = size.height / 2;
        this.addChild(menu);

        return true;
    }
});

DictionaryMenuLayer.create = function () {
    var obj = new DictionaryMenuLayer();
    if (obj && obj.init()) {
        return obj;
    }
    return null;
};