var ResultsLayer = cc.Layer.extend({
    fontColor: cc.color(51, 153, 204, 255),
    newGameButton: true,
    /**
     * @param data
     * data = {
     *  win: bool,          - победа или проигрыш
     *  gamesPlayed: int    - сыграно игр
     *  score: int          - набрано очков
     *  gameTime: int       - затраченное время на игру
     *  gameTimeTitle: str  - надпись для затраченного времени
     *  wordsTotal: int     - найдено слов
     *  foundWords: str[]   - массив найденных слов
     * }
     */
    ctor: function (data) {
        this._super();
        if (data.hasOwnProperty('fontColor')) {
            this.fontColor = data.fontColor;
        }
        var newY;
        var padding = 10;
        var size = cc.director.getWinSize();
        //this.initWithViewSize(size, container);
        //var background = new cc.Sprite(res.Background_png);
        //background.x = size.width / 2;
        //background.y = size.height / 2;
        //this.addChild(background);

        if (data.newGameButton !== undefined) {
            this.newGameButton = data.newGameButton;
        }

        var title = data.title || t('Results');
        if (data.win === true) {
            title = t('Won');
        } else if (data.win === false) {
            title = t('Lose');
        }
        var titleLabel = new cc.LabelTTF(title, "Intro", 55);
        titleLabel.setColor(cc.color(213, 213, 213, 255));
        titleLabel.setPosition(size.width / 2, size.height - titleLabel.height / 2 - padding);
        this.addChild(titleLabel);
        newY = titleLabel.y - titleLabel.height / 2;

        var menu = this.createMainMenu();
        menu.setPosition(size.width / 2, newY - menu.height / 2 - padding);
        this.addChild(menu);
        newY = menu.y - menu.height;

        if (data.hasOwnProperty('wordsTotal')) {

            var lineHoriz = new cc.Sprite('#' + 'line_horiz.png');
            lineHoriz.x = size.width / 2;
            lineHoriz.y = newY;
            this.addChild(lineHoriz);

            newY = lineHoriz.y;
            //время
            if (data.hasOwnProperty('gameTime') && data.hasOwnProperty('gameTimeTitle')) {
                var gameTimeLabel = new cc.LabelTTF(data.gameTimeTitle, "Arial", 18, cc.size(90, 42), cc.TEXT_ALIGNMENT_LEFT);
                gameTimeLabel.x = size.width / 4;
                gameTimeLabel.y = newY - gameTimeLabel.height / 2 - padding;
                gameTimeLabel.setColor(this.fontColor);
                this.addChild(gameTimeLabel);
                var gameTimeValue = new cc.LabelTTF(data.gameTime, "Intro", 40);
                gameTimeValue.x = size.width / 2 - gameTimeValue.width / 2;
                gameTimeValue.y = gameTimeLabel.y;
                gameTimeValue.setColor(cc.color(213, 213, 213, 255));
                this.addChild(gameTimeValue);
                newY = gameTimeLabel.y - gameTimeLabel.height / 2;
            }
            //найдено слов
            var wordsTotalLabel = new cc.LabelTTF(t('Words found') + ':', "Arial", 18, cc.size(90, 42), cc.TEXT_ALIGNMENT_LEFT);
            wordsTotalLabel.x = size.width / 4;
            wordsTotalLabel.y = newY - wordsTotalLabel.height / 2 - padding;
            wordsTotalLabel.setColor(this.fontColor);
            this.addChild(wordsTotalLabel);
            var wordsTotal = new cc.LabelBMFont(data.wordsTotal, res.FntIntro40White);
            wordsTotal.x = size.width / 2 - wordsTotal.width / 2;
            wordsTotal.y = wordsTotalLabel.y;
            wordsTotal.setColor(cc.color(213, 213, 213, 255));
            this.addChild(wordsTotal);
            newY = wordsTotalLabel.y - wordsTotalLabel.height / 2;
            //набрано очков
            if (parseInt(data.score) > 0) {
                var gameScoreLabel = new cc.LabelTTF(t('Score') + ':', "Arial", 18, cc.size(90, 42), cc.TEXT_ALIGNMENT_LEFT);
                gameScoreLabel.x = size.width / 4;
                gameScoreLabel.y = newY - gameScoreLabel.height / 2 - padding;
                gameScoreLabel.setColor(this.fontColor);
                this.addChild(gameScoreLabel);
                var gameScore = new cc.LabelBMFont(data.score, res.FntIntro40White);
                gameScore.x = size.width / 2 - gameScore.width / 2;
                gameScore.y = gameScoreLabel.y;
                gameScore.setColor(cc.color(213, 213, 213, 255));
                this.addChild(gameScore);
                newY = gameScoreLabel.y - gameScoreLabel.height / 2;
            }
            //Количество сыгранных игр
            var hexagonColor = new cc.Sprite('#' + 'hexagon_color.png');
            hexagonColor.x = size.width * 3 / 4;
            hexagonColor.y = lineHoriz.y - hexagonColor.height / 2 - padding;
            this.addChild(hexagonColor);
            var gamesPlayed = new cc.LabelBMFont(data.gamesPlayed, res.FntIntro40White);
            gamesPlayed.x = hexagonColor.x;
            gamesPlayed.y = hexagonColor.y + gamesPlayed.height / 4;
            this.addChild(gamesPlayed);
            var gamesPlayedLabel1 = new cc.LabelTTF(t('Played'), "Arial", 16);
            gamesPlayedLabel1.x = hexagonColor.x;
            gamesPlayedLabel1.y = hexagonColor.y - gamesPlayedLabel1.height / 2 - 7;
            this.addChild(gamesPlayedLabel1);
            var gamesPlayedLabel2 = new cc.LabelTTF(t('games'), "Arial", 16);
            gamesPlayedLabel2.x = hexagonColor.x;
            gamesPlayedLabel2.y = gamesPlayedLabel1.y - gamesPlayedLabel2.height / 2 - 7;
            this.addChild(gamesPlayedLabel2);
            if ((hexagonColor.y - hexagonColor.height / 2) < newY) {
                newY = hexagonColor.y - hexagonColor.height / 2;
            }

            var lineHoriz2 = new cc.Sprite('#' + 'line_horiz.png');
            lineHoriz2.x = size.width / 2;
            lineHoriz2.y = newY - padding;
            this.addChild(lineHoriz2);
            newY = lineHoriz2.y - padding;

            if (data.wordsTotal > 0) {
                var items = [];
                var num = 1;
                var descriptionLabel = null;
                for (var i in data.foundWords) {
                    descriptionLabel = null;
                    var score = data.foundWords[i].score * data.foundWords[i].count;
                    var rowLayer = new cc.Layer();
                    rowLayer.width = size.width / 3 + padding;
                    //порядковый номер слова
                    var numLabel = new cc.LabelBMFont(num + '.', res.FntIntro20White, 40, cc.TEXT_ALIGNMENT_LEFT);
                    numLabel.width = 40;
                    numLabel.setColor(cc.color(153, 153, 153, 255));
                    numLabel.setPosition(-rowLayer.width / 2 - 20, 4);
                    rowLayer.addChild(numLabel);
                    //найденное слово
                    var wordLabel = new cc.LabelTTF(i, "Arial", 22, cc.size(120, 25), cc.TEXT_ALIGNMENT_LEFT);
                    wordLabel.setFontFillColor(this.fontColor);
                    wordLabel.setPosition(numLabel.x - numLabel.width / 2 + 40 + wordLabel.width / 2, 4);
                    rowLayer.addChild(wordLabel);
                    //значение слова
                    if (typeof DICT_DESCRIPTION !== 'undefined') {
                        var description = DICT_DESCRIPTION[i.toLowerCase()];
                        if (description) {
                            descriptionLabel = new cc.LabelTTF(description, "Arial", 18, cc.size(600, 42), cc.TEXT_ALIGNMENT_LEFT, cc.VERTICAL_TEXT_ALIGNMENT_TOP);
                            descriptionLabel.setFontFillColor(cc.color(200, 200, 200, 255));
                            descriptionLabel.setPosition(numLabel.x - numLabel.width / 2 + 40 + descriptionLabel.width / 2, wordLabel.y - wordLabel.height / 2 - descriptionLabel.height / 2);
                            rowLayer.addChild(descriptionLabel);
                        }
                    }
                    //кол-во заработанных очков
                    var scoreLabel = new cc.LabelBMFont(score, res.FntIntro20White, 40, cc.TEXT_ALIGNMENT_RIGHT);
                    scoreLabel.width = 40;
                    scoreLabel.setColor(cc.color(213, 213, 213, 255));
                    scoreLabel.setPosition(wordLabel.x + wordLabel.width + scoreLabel.width / 2, 4);
                    rowLayer.addChild(scoreLabel);
                    //кнопка рассказать друзьям
                    if (typeof shareFriends === 'function') {
                        var shareButton = new cc.Sprite('#stat_friends_normal.png');
                        shareButton.word = i;
                        var shareButtonSelected = new cc.Sprite('#stat_friends_selected.png');
                        var shareButtonItem = new cc.MenuItemSprite(shareButton, shareButtonSelected, null, function () {
                            var message;
                            shareFriends(this.word);
                        }, shareButton);
                        var shareButtonMenu = new cc.Menu(shareButtonItem);
                        shareButtonMenu.setPosition(scoreLabel.x + scoreLabel.width / 2 + shareButton.width / 2 + padding / 2, 9);
                        rowLayer.addChild(shareButtonMenu);
                    }

                    //rowLayer.width = numLabel.width + wordLabel.width + scoreLabel.width + shareButton.width + padding;
                    rowLayer.width = 675;
                    rowLayer.height = Math.max(numLabel.height, wordLabel.height, scoreLabel.height, 25);
                    if (typeof DICT_DESCRIPTION !== 'undefined') {
                        rowLayer.height = rowLayer.height * 2 + 10;
                    }
                    items.push(rowLayer);
                    num++;
                }
                if (typeof DICT_DESCRIPTION == 'undefined') {
                    this.addItemsInRows(items, newY - padding * 2, Math.ceil(items.length / 2), Math.floor(items.length / 2));
                } else {
                    this.addItemsInRows(items, newY - padding * 2, items.length);
                }
            }
        }
        this.mouseEventListener = cc.EventListener.create({
            event: cc.EventListener.MOUSE,
            onMouseScroll: function (event) {
                var size = cc.director.getWinSize();
                var target = event.getCurrentTarget();

                if (target.height <= size.height) {
                    return;
                }

                var currentY = target.y;
                var newY = currentY - event.getScrollY();
                var maxY = 0;//size.height - target.height / 2;
                var minY = (target.height - size.height);
                if (newY < maxY) {
                    newY = maxY;
                } else if (newY > minY) {
                    newY = minY;
                }
                target.y = newY;
            }.bind(this)
        });
        cc.eventManager.addListener(this.mouseEventListener, this);
    },
    init: function () {

    },
    /**
     * align menu items in rows
     * @example
     * // Example
     * menu.alignItemsInRows(5,3)//this will align items to 2 rows, first row with 5 items, second row with 3
     *
     * menu.alignItemsInRows(4,4,4,4)//this creates 4 rows each have 4 items
     * @param items
     * @param startY
     */
    addItemsInRows: function (items, startY/*Multiple arguments*/) {
        if ((arguments.length > 0) && (arguments[arguments.length - 1] == null))
            cc.log("parameters should not be ending with null in Javascript");
        var columns = [], i;
        for (i = 2; i < arguments.length; i++) {
            columns.push(arguments[i]);
        }
        var columnWidths = [];
        var columnHeights = [];

        var winSize = cc.director.getWinSize();
        var width = 0;
        var column = 0;
        var columnWidth = 675 / columns.length - 40 * (columns.length > 1 ? 1 : 0);//winSize.width / 2;
        var columnHeight = items[0].height;
        var rowsOccupied = 0;
        var columnRows, child, len, tmp;

        if (items.length > 0) {
            for (i = 0, len = items.length; i < len; i++) {
                child = items[i];
                // check if too many menu items for the amount of rows/columns
                if (column >= columns.length)
                    continue;

                columnRows = columns[column];
                // can't have zero rows on a column
                if (!columnRows)
                    continue;

                // columnWidth = fmaxf(columnWidth, [item contentSize].width);
                tmp = child.width;
                //columnWidth = ((columnWidth >= tmp || isNaN(tmp)) ? columnWidth : tmp);

                //columnHeight += (child.height + 5);
                ++rowsOccupied;

                if (rowsOccupied >= columnRows) {
                    columnWidths.push(columnWidth);
                    columnHeights.push(columnHeight);
                    width += columnWidth + 10;

                    rowsOccupied = 0;
                    //columnWidth = 0;
                    //columnHeight = -5;
                    ++column;
                }
            }
        }
        // check if too many rows/columns for available menu items.
        //cc.assert(!rowsOccupied, "");

        column = 0;
        //columnWidth = 0;
        columnRows = 0;
        var x = winSize.width / 2;
        var y = 0.0;
        var minY = 0;

        if (items.length > 0) {
            for (i = 0, len = items.length; i < len; i++) {
                child = items[i];
                if (columnRows == 0) {
                    columnRows = columns[column];
                    y = columnHeights[column];
                }

                // columnWidth = fmaxf(columnWidth, [item contentSize].width);
                tmp = child._getWidth();
                //columnWidth = ((columnWidth >= tmp || isNaN(tmp)) ? columnWidth : tmp);

                var posX = x - (winSize.width - child.width) - 10;//x - child.width / 2 - 10;
                //console.log(column);
                //if (column == 0) {
                //    posX += 20;
                //} else {
                //    posX -= 20;
                //}
                child.setPosition(posX, startY + y - child.height);
                this.addChild(child);
                y -= child.height + 10;
                if (child.y < minY) {
                    minY = child.y;
                }
                ++rowsOccupied;

                if (rowsOccupied >= columnRows) {
                    x += columnWidth + 30;
                    rowsOccupied = 0;
                    columnRows = 0;
                    //columnWidth = 0;
                    ++column;
                }
            }
        }
        if (minY < 0) {
            this.height = winSize.height - minY + columnHeight;
        }
    },
    createMainMenu: function () {
        var items = [];
        var fontSize = 25;
        var fontColor = cc.color(50, 65, 68, 255);
        var menuNormalSprite = new cc.Sprite('#' + res.sprite.menuNormal);
        var mainMenuButton = new cc.MenuItemSprite(
            menuNormalSprite,
            new cc.Sprite('#' + res.sprite.menuSelected),
            null,
            function () {
                cc.LoaderScene.preload(g_menu, function () {
                    var scene = MainMenu.scene();
                    cc.director.runScene(scene);
                }, this);
            }
        );
        var mainMenuText = new cc.LabelBMFont(t('Main menu'),res.FntIntro25Gray);
        mainMenuText.x = menuNormalSprite.width / 2;
        mainMenuText.y = menuNormalSprite.height / 2;
        mainMenuButton.addChild(mainMenuText);
        items.push(mainMenuButton);

        if (this.newGameButton) {
            var newGameButton = new cc.MenuItemSprite(
                new cc.Sprite('#' + res.sprite.menuNormal),
                new cc.Sprite('#' + res.sprite.menuSelected),
                null,
                function () {
                    this.removeAllChildren(true);
                    this.removeFromParent(true);
                    cc.eventManager.dispatchCustomEvent(EVENT_NEW_GAME);
                }.bind(this)
            );
            var newGameText = new cc.LabelBMFont(t('New game'), res.FntIntro25Gray);
            newGameText.x = newGameButton.width / 2;
            newGameText.y = newGameButton.height / 2;
            newGameButton.addChild(newGameText);
            items.push(newGameButton);
        }
        var menu = new cc.Menu(items);
        var padding = 10;
        menu.alignItemsHorizontallyWithPadding(padding);
        menu.width = mainMenuButton.width * menu._children.length + padding * (menu._children.length - 1);
        menu.height = mainMenuButton.height;

        return menu;
    }
});

ResultsLayer.create = function (data) {
    return new ResultsLayer(data);
};