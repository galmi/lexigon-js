var HudLayer = cc.LayerColor.extend({
    pauseEnabled: true,
    timerEnabled: false,
    endButtonEnabled: false,
    pauseButton: null,
    endButton: null,
    bgColor: cc.color(50, 66, 69, 128),
    paddingWidth: 20,
    paddingHeight: 20,
    init: function (params) {
        for (var i in params) {
            this[i] = params[i];
        }

        this.setColor(cc.color(50, 66, 69, 128));
        this.setOpacity(150);
        var height = 0;

        var items = [];

        var menuWidth = 0;
        var menuHeight = 0;
        if (this.endButtonEnabled) {
            this.endButtonSprite = new cc.Sprite();
            this.endButtonSprite.initWithSpriteFrameName(this.endButton);
            var endButtonSpriteSelected = new cc.Sprite();
            endButtonSpriteSelected.initWithSpriteFrameName("end_button_selected.png");
            items.push(new cc.MenuItemSprite(this.endButtonSprite, endButtonSpriteSelected, null, function () {
                if (confirm(t('Do you want end the game?'))) {
                    cc.eventManager.dispatchCustomEvent(EVENT_GAME_RESUMED);
                    cc.eventManager.dispatchCustomEvent(EVENT_GAME_END);
                }
            }.bind(this)));
            menuWidth += this.endButtonSprite.width;
            menuHeight = this.endButtonSprite.height;
        }

        if (this.pauseEnabled) {
            this.pauseSprite = new cc.Sprite('#'+this.pauseButton);
            var pauseSpriteSelected = new cc.Sprite();
            pauseSpriteSelected.initWithSpriteFrameName(res.sprite.pauseButtonSelected);
            items.push(new cc.MenuItemSprite(this.pauseSprite, pauseSpriteSelected, null, function () {
                cc.eventManager.dispatchCustomEvent(EVENT_GAME_PAUSED);
            }.bind(this)));
            menuWidth += this.pauseSprite.width;
            if (this.pauseSprite.height > menuHeight) {
                menuHeight = this.pauseSprite.height;
            }

            var hint = new cc.Sprite('#hint.png');
            var hintSelected = new cc.Sprite('#hint.png');
            items.push(new Hint(hint, hintSelected));
            menuWidth += hint.width;
        }

        this.menu = new cc.Menu(items);
        this.menu.alignItemsHorizontallyWithPadding(this.paddingWidth);
        menuWidth += this.paddingWidth * (items.length - 1);
        this.menu.width = menuWidth;
        this.menu.height = menuHeight;
        this.addChild(this.menu);

        height = this.pauseSprite.height + this.paddingHeight;

        if (this.timerEnabled) {
            this.timerLabel = Timer.create(20);
            this.addChild(this.timerLabel);
            if (this.timerLabel.height > this.pauseSprite.height) {
                height = this.timerLabel.height;
            }
            this.timerLabel.addLoadedEventListener(function() {
                if (this.timerLabel.height > this.pauseSprite.height) {
                    height = this.timerLabel.height;
                }
                this.changeWidthAndHeight(this.calcWidth(), height);
                var size = cc.director.getWinSize();
                this.x = size.width - this.width;
                this.y = size.height - this.height;
                this.updatePositions();
            }, this);
        }
        this.changeWidthAndHeight(this.calcWidth(), height);

        this.updatePositions();

        return true;
    },
    updatePositions: function () {
        this.menu.x = this.width - this.menu.width / 2 - this.paddingWidth / 2;
        this.menu.y = this.height - this.menu.height / 2 - this.paddingHeight / 2;
        if (this.timerLabel) {
            this.timerLabel.x = this.timerLabel.width / 2 + this.paddingWidth/2;
            this.timerLabel.y = this.timerLabel.height / 2;
        }
    },
    calcWidth: function () {
        var width = this.menu.width;
        if (this.timerLabel) {
            width += this.timerLabel.width + this.paddingWidth / 2;
        }

        return width + this.paddingWidth;
    }
});

HudLayer.create = function (params) {
    var obj = new HudLayer();
    if (obj && obj.init(params)) {
        return obj;
    }
    return null;
};