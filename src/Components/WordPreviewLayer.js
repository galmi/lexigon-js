var WordPreviewLayer = cc.LayerColor.extend({
    listeners: [],
    _instance: null,
    init: function () {
        if (this._super()) {
            var cmp = this;
            this.setColor(cc.color(0, 0, 0, 255));
            this.setContentSize(0, 0);
            this.label = new cc.LabelBMFont("0", res.FntIntro40White);
            this.addChild(this.label);
            this.listeners.push(cc.eventManager.addCustomListener(EVENT_NEW_WORD, function () {
                this.setOpacity(255);
                for (var i in cmp.children) {
                    this.children[i].setOpacity(255);
                }
            }.bind(this)));
            this.listeners.push(cc.eventManager.addCustomListener(EVENT_END_WORD, function () {
                var fadeOut = cc.fadeOut(.2);
                this.runAction(fadeOut);
                for (var i in this.children) {
                    this.children[i].runAction(fadeOut.clone());
                }
            }.bind(this)));
            this.listeners.push(cc.eventManager.addCustomListener(EVENT_UPDATE_WORD, function (data) {
                data = data.getUserData();
                this.updateText(data.word, data.exists);
            }.bind(this)));
            return true;
        }
        return false;
    },
    updateText: function (word, exists) {
        var padding = 10;
        this.label.setString(word);
        if (exists) {
            this.label.setColor(cc.color(255, 255, 255, 255));
        } else {
            this.label.setColor(cc.color(200, 200, 200, 255));
        }
        var size = cc.director.getWinSize();
        this.setContentSize(this.label.width + padding, this.label.height + padding);
        this.setPositionX(size.width / 2 - this.width / 2);
        this.setPositionY(size.height - this.height);
        this.label.setPositionX(this.label.width / 2 + padding / 2);
        this.label.setPositionY(this.height / 2 - 2);
    },
    cleanup: function() {
        this._super();
        for (var i in this.listeners) {
            cc.eventManager.removeListener(this.listeners[i]);
        }
    }
});

WordPreviewLayer.getInstance = function () {
    if (this._instance == null) {
        this._instance = WordPreviewLayer.create();
    }
    return this._instance;
};

WordPreviewLayer.create = function () {
    var obj = new WordPreviewLayer();
    if (obj && obj.init()) {
        return obj;
    }
    return null;
};