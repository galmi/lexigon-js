var Timer = cc.LabelBMFont.extend({
    started: false,
    paused: false,
    lastTime: null,
    timerLength: 0,
    alert: 20,
    listeners: [],
    init: function () {
        this.listeners.push(cc.eventManager.addCustomListener(EVENT_TIMER_START, function (data) {
            this.start(data.getUserData());
        }.bind(this)));
        this.listeners.push(cc.eventManager.addCustomListener(EVENT_TIMER_STOP, function () {
            this.stop();
        }.bind(this)));
        this.listeners.push(cc.eventManager.addCustomListener(EVENT_TIMER_PAUSE, function () {
            this.pause();
        }.bind(this)));
        this.listeners.push(cc.eventManager.addCustomListener(EVENT_TIMER_CONTINUE, function () {
            this.resume();
        }.bind(this)));
        this.listeners.push(cc.eventManager.addCustomListener(EVENT_TIMER_ADD, function (data) {
            var seconds = data.getUserData();
            this.add(seconds);
            cc.eventManager.dispatchCustomEvent(EVENT_TIMER, this.timerLength);
        }.bind(this)));
        this.listeners.push(cc.eventManager.addCustomListener(EVENT_TIMER_END, function (data) {
            this.timerLength = 0;
            this.stop();
        }.bind(this)));
        return true;
    },
    cleanup: function() {
        this._super();
        for (var i in this.listeners) {
            cc.eventManager.removeListener(this.listeners[i]);
        }
    },
    add: function (seconds) {
        this.timerLength += seconds;
    },
    update: function () {
        var curTime = new Date().getTime() / 1000;
        var delta = 0;
        if (!this.paused) {
            delta = curTime - this.lastTime;
        }
        this.lastTime = curTime;
        this.timerLength -= delta;
        if (this.timerLength <= this.alert) {
            this.setColor(cc.color(255, 0, 0, 255));
        } else {
            this.setColor(cc.color(255, 255, 255, 255));
        }
        if (this.timerLength <= 0) {
            cc.eventManager.dispatchCustomEvent(EVENT_TIMER_END);
        }
        if (this.timerLength <= 10) {
            var seconds = Math.round(this.timerLength * 100) / 100;
            this.setString(seconds);
        } else {
            this.setString(this.timerLength.toMMSS());
        }
    },
    start: function (seconds) {
        this.add(seconds);
        this.started = true;
        this.paused = false;
        this.lastTime = new Date().getTime() / 1000;
        this.schedule(this.update);
    },
    pause: function () {
        this._super();
        this.paused = true;
    },
    resume: function () {
        this._super();
        this.lastTime = new Date().getTime() / 1000;
        this.paused = false;
    },
    stop: function () {
        this.unschedule(this.update);
    }
});

Timer.getInstance = function () {
    return Timer._instance;
};

Timer.create = function (alert) {
    var obj = new Timer("00:00", res.FntIntro40White);
    if (alert) {
        obj.alert = alert;
    }
    if (obj && obj.init()) {
        Timer._instance = obj;
        return obj;
    }
    return null;
};