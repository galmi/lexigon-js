var HintsMenuLayer = cc.LayerColor.extend({
    init: function () {
        var fontSize = 25;
        var fontColor = cc.color(255, 255, 0, 255);
        var menuItems = [];
        var size = cc.director.getWinSize();
        this.setColor(cc.color(0, 0, 0, 200));
        this.setContentSize(size.width, size.height);

        var background = new cc.Sprite(res.Background_png);
        background.x = size.width / 2;
        background.y = size.height / 2;
        this.addChild(background);

        var title = new cc.LabelTTF(t('Buy hints'), 'Intro', 50);
        title.setFontFillColor(cc.color(214, 213, 213));
        title.x = size.width / 2;
        title.y = size.height - title.height / 2 - 20;
        this.addChild(title);

        var hints5Button = new cc.MenuItemSprite(
            new cc.Sprite('#' + res.sprite.menuNormal),
            new cc.Sprite('#' + res.sprite.menuSelected),
            null,
            function () {
                cc.eventManager.dispatchCustomEvent(EVENT_BUY_HINTS, 5);
            }
        );
        var text = new cc.LabelBMFont(t('Buy %d hints for %d%s', 5, HINTS[5].price, HINTS[5].currency), res.FntIntro20White);
        text.setColor(fontColor);
        text.x = hints5Button.width / 2;
        text.y = hints5Button.height / 2;
        hints5Button.addChild(text);
        menuItems.push(hints5Button);

        var hints10Button = new cc.MenuItemSprite(
            new cc.Sprite('#' + res.sprite.menuNormal),
            new cc.Sprite('#' + res.sprite.menuSelected),
            null,
            function () {
                cc.eventManager.dispatchCustomEvent(EVENT_BUY_HINTS, 10);
            }
        );
        text = new cc.LabelBMFont(t('Buy %d hints for %d%s', 10, HINTS[10].price, HINTS[10].currency), res.FntIntro20White);
        text.setColor(fontColor);
        text.x = hints10Button.width / 2;
        text.y = hints10Button.height / 2;
        hints10Button.addChild(text);
        menuItems.push(hints10Button);

        var hints15Button = new cc.MenuItemSprite(
            new cc.Sprite('#' + res.sprite.menuNormal),
            new cc.Sprite('#' + res.sprite.menuSelected),
            null,
            function () {
                cc.eventManager.dispatchCustomEvent(EVENT_BUY_HINTS, 15);
            }
        );
        text = new cc.LabelBMFont(t('Buy %d hints for %d%s', 15, HINTS[15].price, HINTS[15].currency), res.FntIntro20White);
        text.setColor(fontColor);
        text.x = hints15Button.width / 2;
        text.y = hints15Button.height / 2;
        hints15Button.addChild(text);
        menuItems.push(hints15Button);

        var returnButton = new cc.MenuItemSprite(
            new cc.Sprite('#' + res.sprite.menuNormal),
            new cc.Sprite('#' + res.sprite.menuSelected),
            null,
            function () {
                cc.eventManager.dispatchCustomEvent(EVENT_GAME_RESUMED);
            }
        );
        text = new cc.LabelBMFont(t('Continue'), res.FntIntro25Gray);
        text.x = returnButton.width / 2;
        text.y = returnButton.height / 2;
        returnButton.addChild(text);
        menuItems.push(returnButton);

        var menu = new cc.Menu(menuItems);
        menu.alignItemsVerticallyWithPadding(10);
        menu.x = size.width / 2;
        menu.y = size.height / 2;
        this.addChild(menu);

        return true;
    }
});

HintsMenuLayer.create = function () {
    var obj = new HintsMenuLayer();
    if (obj && obj.init()) {
        return obj;
    }
    return null;
};