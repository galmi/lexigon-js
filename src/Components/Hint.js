var Hint = cc.MenuItemSprite.extend({
    hintLabel: null,
    listeners: [],
    ctor: function(normalSprite, selectedSprite) {
        this._super(normalSprite, selectedSprite, null, function(){
            this.search();
        }.bind(this));

        this.hintLabel = new cc.LabelTTF('0', 'Arial', 11);
        this.addChild(this.hintLabel);
        this.updateHints();

        this.listeners.push(cc.eventManager.addCustomListener(EVENT_HINTS_ADDED, function(data){
            cc.UserData['hints'] += parseInt(data.getUserData());
            this.updateHints();
        }.bind(this)));

        this.listeners.push(cc.eventManager.addCustomListener(EVENT_HINTS_UPDATE, function(){
            this.updateHints();
        }.bind(this)));
    },
    cleanup: function() {
        this._super();
        for (var i in this.listeners) {
            cc.eventManager.removeListener(this.listeners[i]);
        }
    },
    search: function () {
        try {
            ga('send', 'event', 'game', 'hint');
        } catch (e) {
        }
        //Рекурсивная функция - возвращает массив букв рядом я ячейкой с ограничением по радиусу
        var getNeighbors = function (game, x, y, level, neighbors, limitLevel) {
            var isEven = x % 2;
            for (var i = 0; i < Neighbor[isEven].length; i++) {
                var newX = x + Neighbor[isEven][i][0];
                var newY = y + Neighbor[isEven][i][1];
                if (newX >= 0 && newY >= 0 && newX < game.size && newY < game.size) {
                    var tileXY = game.table[newX][newY];
                    if (typeof tileXY == 'object' && neighbors.indexOf(tileXY) == -1) {
                        neighbors.push(tileXY);
                    }
                    if (level < limitLevel) {
                        getNeighbors(game, newX, newY, level + 1, neighbors, limitLevel);
                    }
                }
            }
        };
        //рекурсивная функция поиска слов в словаре (отличается от той что в Game/Abstract, исключая поиск в глубину, когда достигли дна)
        var findTrieWord = function (word, cur) {
            cur = cur || Dict;
            for (var node in cur) {
                if (word.indexOf(node) === 0) {
                    var val = typeof cur[node] === "number" && cur[node] ?
                        Dict.$[cur[node]] :
                        cur[node];
                    if (node.length === word.length) {
                        if (val === 0 || val.$ === 0) {
                            return true;
                        } else {
                            return word.length;
                        }
                    } else {
                        return findTrieWord(word.slice(node.length), val);
                    }
                }
            }

            return false;
        };
        //Рекурсивная функция поиска слова с проверкой касания пустой ячейки на поле
        var hint = function (game, letter, visited, level, word) {
            var levelLimit = 7;
            word += letter.letter;
            var trieWord = findTrieWord(word.toLowerCase());
            if (trieWord === true) {
                return word;
            }
            if (trieWord === false || level >= levelLimit) {
                return false;
            }
            var neighbors = [];
            getNeighbors(game, letter.tableX, letter.tableY, 1, neighbors, 1);
            for (var i in neighbors) {
                if (visited.indexOf(neighbors[i]) != -1) {
                    continue;
                }
                visited.push(neighbors[i]);
                var foundedWord = hint(game, neighbors[i], visited, level + 1, word);
                if (foundedWord) {
                    var hasasterisk = false;
                    for (i in visited) {
                        if (game.hasNeighbor(visited[i].tableX, visited[i].tableY, '*')) {
                            hasAsterisk = true;
                            break;
                        }
                    }
                }
                if (hasAsterisk) {
                    return foundedWord;
                } else {
                    visited.pop();
                }
            }
        };

        var scene = cc.director.getRunningScene();
        var i;
        if (scene && typeof scene.game != "undefined") {
            //Проверяем наличие у пользователя подсказок
            if (cc.UserData['hints'] > 0) {
                //Если есть подсказки, ищем слово
                var game = scene.game;
                var neighbors = [];
                for (i = 0; i < game.table.length; i++) {
                    for (var j = 0; j < game.table.length; j++) {
                        if (game.table[i][j] == '*') {
                            getNeighbors(game, i, j, 1, neighbors, 4);
                        }
                    }
                }
                var foundedWord = '';
                for (var item in neighbors) {
                    var word = '';
                    var visited = [neighbors[item]];
                    var hasAsterisk = false;
                    foundedWord = hint(game, neighbors[item], visited, 1, word);
                    if (foundedWord) {
                        break;
                    }
                }

                if (foundedWord) {
                    //ОБновляем счетчик
                    cc.UserData['hints']--;
                    this.updateHints();

                    //Выводим подсказку
                    cc.eventManager.dispatchCustomEvent(EVENT_HINT_FOUNDED, null);
                    cc.eventManager.dispatchCustomEvent(EVENT_SHOW_INFO, t('Hinted word "%s"', foundedWord));

                    //Смещаем поле в позицию гексагона
                    cc.eventManager.dispatchCustomEvent(EVENT_CENTER_SCENE, visited[0]);

                    //Выделяем гексагоны
                    var selectedFunc = [];
                    var pauseDelta = .5;
                    if (game instanceof GameSurvive) {
                        pauseDelta = .3;
                    }
                    for (i in visited) {
                        var pause = cc.actionInterval(pauseDelta * i);
                        pause.update = function () {
                        };
                        var func1 = new cc.CallFunc(function() {this.selectSprite()}, visited[i]);
                        selectedFunc.push(new cc.CallFunc(function() {this.hasWordSprite()}, visited[i]));
                        visited[i].runAction(cc.sequence(pause, func1));
                    }
                    pause = cc.actionInterval(pauseDelta * visited.length);
                    pause.update = function () {};
                    visited[i].runAction(cc.sequence([pause].concat(selectedFunc)));
                } else {
                    alert(t('Words was not found'));
                }
            } else {
                //Показываем окно для покупки подсказок
                if (typeof HINTS != "undefined") {
                    cc.eventManager.dispatchCustomEvent(EVENT_BUY_HINTS_MENU);
                } else {
                    cc.eventManager.dispatchCustomEvent(EVENT_BUY_HINTS, null);
                }
            }
        }
    },
    updateHints: function() {
        var hintsCount = parseInt(cc.UserData['hints']);
        this.hintLabel.setString(hintsCount);
        this.hintLabel.setColor(cc.color(255, 0, 0, 255));
        this.hintLabel.setPositionY(this.hintLabel.height/2);
        this.hintLabel.setPositionX(this.width - this.hintLabel.width/2);
    }
});

