var DICTS = {
    en: {
        lettersFreq: {
            "A": 46,
            "R": 41,
            "D": 18,
            "V": 6,
            "K": 5,
            "B": 11,
            "C": 24,
            "U": 22,
            "S": 31,
            "F": 8,
            "T": 38,
            "N": 35,
            "O": 35,
            "E": 61,
            "H": 15,
            "I": 42,
            "Y": 10,
            "M": 17,
            "G": 13,
            "L": 28,
            "J": 1,
            "Z": 2,
            "P": 17,
            "W": 6,
            "Q": 2,
            "X": 2
        },
        bombLetters: ["J", "Z", "Q", "X"],
        dict: "/res/eng-trie.json",
        dictFull: "/res/eng_full.js",
        title: 'EN',
        tutorialTable:[
            [null,  null,   null,   "W",    "Q",    "K",    "B",    "H",    "H",    null,   null],
            [null,  null,   "T",    "E",    "O",    "A",    "O",    "O",    "S",    null,   null],
            [null,  null,   "C",    "L",    "T",    "B",    "M",    "P",    "Z",    "E",    null],
            [null,  "P",    "E",    "O",    "I",    "T",    "E",    "L",    "E",    "P",    null],
            [null,  "C",    "B",    "O",    "U",    "Q",    "M",    "X",    "P",    "I",    "E" ],
            ["Z",   "A",    "E",    "E",    "E",    "*",    "I",    "P",    "U",    "F",    "U" ],
            [null,  "Y",    "E",    "B",    "Ч",    "G",    "A",    "I",    "P",    "Y",    "A" ],
            [null,  "O",    "E",    "Z",    "E",    "M",    "R",    "B",    "M",    "M",    null],
            [null,  null,   "С",    "E",    "I",    "Z",    "E",    "E",    "O",    "G",    null],
            [null,  null,   "С",    "T",    "T",    "D",    "G",    "I",    "N",    null,   null],
            [null,  null,   null,   "A",    "Б",    "K",    "O",    "O",    "O",    null,   null]
        ],
        tutorialBombLetters: ['X'],
        tutorialQuest1word: 'GAME',
        tutorialQuest1Action: function() {
            this.runAction(cc.sequence(
                cc.delayTime(2), cc.callFunc(function () {
                    this.game.table[6][5].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[6][6].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[7][5].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(
                    function () {
                        this.game.table[6][5].hasWordSprite();
                        this.game.table[6][6].hasWordSprite();
                        this.game.table[7][5].hasWordSprite();
                        this.game.table[7][4].hasWordSprite();
                    }, this)
            ));
        },
        tutorialQuest3word: 'EXPIRE',
        tutorialQuest3Action: function() {
            this.runAction(cc.sequence(
                cc.delayTime(2), cc.callFunc(function () {
                    this.game.table[3][6].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[4][7].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[5][7].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[6][7].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[7][6].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[3][6].hasWordSprite();
                    this.game.table[4][7].hasWordSprite();
                    this.game.table[5][7].hasWordSprite();
                    this.game.table[6][7].hasWordSprite();
                    this.game.table[7][6].hasWordSprite();
                    this.game.table[8][6].hasWordSprite();
                }, this)
            ));
        },
        tutorialQuest4word: 'BEGIN',
        tutorialQuest4Action: function() {
            this.runAction(cc.sequence(
                cc.delayTime(2), cc.callFunc(function () {
                    this.game.table[7][7].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[8][7].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[9][6].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[9][7].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[7][7].hasWordSprite();
                    this.game.table[8][7].hasWordSprite();
                    this.game.table[9][6].hasWordSprite()
                    this.game.table[9][7].hasWordSprite()
                    this.game.table[9][8].hasWordSprite();
                }, this)
            ));
        }
    },
    ru: {
        lettersFreq: {
            "А": 239,
            "Б": 42,
            "Ж": 19,
            "У": 58,
            "Р": 161,
            "Н": 178,
            "Ы": 70,
            "Й": 64,
            "Ч": 41,
            "И": 206,
            "К": 106,
            "З": 50,
            "Е": 191,
            "Ц": 18,
            "С": 155,
            "Т": 209,
            "В": 124,
            "О": 247,
            "Я": 59,
            "Г": 34,
            "Л": 103,
            "Ь": 115,
            "М": 55,
            "Д": 61,
            "Ю": 8,
            "Щ": 12,
            "Х": 16,
            "Ш": 21,
            "П": 93,
            "Ф": 12,
            "Ъ": 1,
            "Э": 4
        },
        bombLetters: ['Ф', 'Х', 'Ц', 'Ш', 'Щ', 'Ъ', 'Э', 'Ю'],
        dict: "/res/efremova-trie.json",
        dictFull: "/res/efremova_full.js",
        title: 'РУ',
        tutorialTable:[
            [null,  null,   null,   "Щ",    "З",    "П",    "В",    "Н",    "Н",    null,   null],
            [null,  null,   "Т",    "У",    "О",    "Д",    "О",    "О",    "Ж",    null,   null],
            [null,  null,   "Ц",    "Л",    "Т",    "В",    "М",    "Р",    "Ж",    "Е",    null],
            [null,  "Р",    "Е",    "О",    "И",    "Т",    "Ь",    "Л",    "Е",    "Р",    null],
            [null,  "Ч",    "Б",    "О",    "У",    "Й",    "М",    "Х",    "Р",    "И",    "Е" ],
            ["З",   "А",    "Е",    "Е",    "Е",    "*",    "Ь",    "Р",    "У",    "Ф",    "У" ],
            [null,  "Я",    "Е",    "В",    "Ч",    "И",    "Г",    "О",    "Р",    "Я",    "А" ],
            [null,  "О",    "Е",    "З",    "А",    "Р",    "М",    "Т",    "М",    "М",    null],
            [null,  null,   "С",    "Ь",    "И",    "З",    "Ь",    "Р",    "О",    "Г",    null],
            [null,  null,   "С",    "Т",    "Т",    "Д",    "П",    "Д",    "Н",    null,   null],
            [null,  null,   null,   "А",    "Б",    "К",    "О",    "О",    "О",    null,   null]
        ],
        tutorialBombLetters: ['Х'],
        tutorialQuest1word: 'ИГРА',
        tutorialQuest1Action: function() {
            this.runAction(cc.sequence(
                cc.delayTime(2), cc.callFunc(function () {
                    this.game.table[6][5].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[6][6].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[7][5].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(
                    function () {
                        this.game.table[6][5].hasWordSprite();
                        this.game.table[6][6].hasWordSprite();
                        this.game.table[7][5].hasWordSprite();
                        this.game.table[7][4].hasWordSprite();
                    }, this)
            ));
        },
        tutorialQuest3word: 'ХРОМ',
        tutorialQuest3Action: function() {
            this.runAction(cc.sequence(
                cc.delayTime(2), cc.callFunc(function () {
                    this.game.table[4][7].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[5][7].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[6][7].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[4][7].hasWordSprite();
                    this.game.table[5][7].hasWordSprite();
                    this.game.table[6][7].hasWordSprite();
                    this.game.table[7][6].hasWordSprite();
                }, this)
            ));
        },
        tutorialQuest4word: 'ТРОН',
        tutorialQuest4Action: function() {
            this.runAction(cc.sequence(
                cc.delayTime(2), cc.callFunc(function () {
                    this.game.table[7][7].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[8][7].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[8][8].selectSprite()
                }, this),
                cc.delayTime(.5), cc.callFunc(function () {
                    this.game.table[7][7].hasWordSprite();
                    this.game.table[8][7].hasWordSprite();
                    this.game.table[8][8].hasWordSprite();
                    this.game.table[9][8].hasWordSprite();
                }, this)
            ));
        }
    }
};

/**
 * Возвращает значение из словаря
 * @param value
 * @returns {*}
 */
function getDictValue(value) {
    var lang = cc.UserData['dict'] || getCookie('dict') || 'ru';
    if (DICTS.hasOwnProperty(lang) && DICTS[lang][value]) {
        return DICTS[lang][value];
    }
    return null;
}

/**
 * Сохранение в куке дефолтного значения словаря
 */
function setUserDict() {
    var dict = getCookie('dict');
    if (dict) {
        cc.UserData['dict'] = dict;
    } else {
        if (navigator.language && navigator.language.toLowerCase().indexOf('en') > -1) {
            cc.UserData['dict'] = 'en';
        } else {
            cc.UserData['dict'] = 'ru';
        }
        setCookie('dict', cc.UserData['dict'], 180);
    }
}

/**
 * Установка значения словаря при выборе пользователем
 * @param dict
 */
function setDict(dict) {
    setCookie('dict', dict, 180);
    cc.UserData['dict'] = dict;
    cc.eventManager.dispatchCustomEvent(EVENT_DICTIONARY_CHANGED);
}