var LetterGen = function () {
    var alphabet = '';
    for (var letter in LETTERS_FREQ) {
        alphabet += letter.repeat(LETTERS_FREQ[letter]);
    }

    this.generate = function () {
        var rnd = Math.floor(Math.random() * alphabet.length);
        return alphabet.substring(rnd, rnd + 1);
    }
};
var GameAbstract = cc.Class.extend({
    //tile: "hexagon_blue.png",
    size: null, //размер игрового поля
    table: [],
    tileWidth: null,
    tileHeight: null,
    tilesCount: 0,
    tilesRemoved: 0,
    alphabet: null,
    score: 0,
    foundWords: {},             //список найденных слов в игре
    gameEnded: false,           //игра окончена
    word: '',                   //собираемое в данный момент слово
    lastComponent: '',          //тайл который был выделен
    components: [],             //список компонентов собранного слова
    neighborRemove: [],         //компоненты для удаления
    listeners: [],
    hexagon_normal: "hexagon_normal.png",
    hexagon_white: "hexagon_white.png",
    hexagon_selected: null,
    hexagon_border: null,
    hexagon_bomb: null,
    init: function () {
        //Игровое поле 31х31 из спрайтов
        this.initListeners();
        this.initTable();

        this.tilesCount = 0;
        this.tilesRemoved = 0;
        this.foundWords = {};
        this.score = 0;
        this.gameEnded = false;
        this.word = '';
        this.lastComponent = '';
        this.components = [];
        this.neighborRemove = [];

        return true;
    },
    initTable: function () {
        var game = this;
        this.alphabet = new LetterGen();
        var filterTable = function (table, index, position) {
            var even = index[0] % 2;
            delete table[index[0]][index[1]];
            game.tilesCount--;
            var newX = index[0] + Neighbor[even][position][0];
            var newY = index[1] + Neighbor[even][position][1];
            if (newX >= 0 && newY >= 0 && newX < game.size && newY < game.size) {
                return filterTable(table, [newX, newY], position);
            }
            return table;
        };
        var middle = Math.ceil(this.size / 2) - 1;
        var i;
        var tileParams = {
            hexagon_normal: this.hexagon_normal,
            hexagon_selected: this.hexagon_selected,
            hexagon_white: this.hexagon_white,
            hexagon_bomb: this.hexagon_bomb,
            hexagon_border: this.hexagon_border
        };
        for (i = 0; i < this.size; i++) {
            this.table[i] = [];
            for (var j = 0; j < this.size; j++) {
                if (middle == i && middle == j) {
                    this.table[i][j] = '*';
                    continue;
                }
                var letter = this.alphabet.generate();
                this.table[i][j] = TileSprite.create(i, j, letter, tileParams);
                this.tilesCount++;
            }
        }
        this.tileWidth = this.table[0][0].width;
        this.tileHeight = this.table[0][0].height;

        var deleteX, deleteY;
        //удаляем левый верхний угол
        if (middle % 2) {
            deleteX = middle - 1;
            deleteY = 0;
        } else {
            deleteX = middle - 2;
            deleteY = 0;
        }
        for (i = deleteX; i >= 0; i--) {
            this.table = filterTable(this.table, [i, deleteY], 3);
        }

        //удаляем правый верхний угол
        if (middle % 2) {
            deleteX = middle + 1;
            deleteY = 0;
        } else {
            deleteX = middle + 2;
            deleteY = 0;
        }
        for (i = deleteX; i < this.size; i++) {
            this.table = filterTable(this.table, [i, deleteY], 5);
        }

        //удаляем левый нижний угол
        if (middle % 2) {
            deleteX = middle - 2;
            deleteY = this.size - 1;
        } else {
            deleteX = middle - 1;
            deleteY = this.size - 1;
        }
        for (i = deleteX; i >= 0; i--) {
            this.table = filterTable(this.table, [i, deleteY], 0);
        }

        //удаляем правый нижний угол
        if (middle % 2) {
            deleteX = middle + 2;
            deleteY = this.size - 1;
        } else {
            deleteX = middle + 1;
            deleteY = this.size - 1;
        }
        for (i = deleteX; i < this.size; i++) {
            this.table = filterTable(this.table, [i, deleteY], 2);
        }
        return this.table;
    },
    gameEnd: function (win) {
        this.gameEnded = true;
        //cc.eventManager.dispatchCustomEvent(EVENT_GAME_END, win);
    },

    addWord: function () {
        if (this.foundWords[this.word] == undefined) {
            this.foundWords[this.word] = {
                score: this.getScoreForWord(this.word),
                count: 1,
                length: this.word.length
            };
        } else {
            this.foundWords[this.word].count++;
        }
        cc.eventManager.dispatchCustomEvent(EVENT_ADDED_WORD, this.word);
    },

    getScoreForLetter: function (letter) {
        return Math.round((1000 - LETTERS_FREQ[letter]) / 10);
    },

    getScoreForWord: function (word) {
        var score = 0;
        for (var i = 0; i < word.length; i++) {
            score += this.getScoreForLetter(word[i]);
        }
        return score;
    },

    addScoreForLetter: function (letter) {
        var score = this.getScoreForLetter(letter);
        this.score += score;
        cc.eventManager.dispatchCustomEvent(EVENT_ADDED_SCORE, score);
        cc.eventManager.dispatchCustomEvent(EVENT_UPDATED_SCORE, this.score);
    },

    isGameEnd: function (components, index) {
        return false;
    },

    hasNeighborExit: function (x, y) {
        if (x == 0 || y == 0 || x == (this.size - 1) || y == (this.size - 1)) {
            return true;
        }
        return this.hasNeighbor(x, y, undefined);
    },
    //есть ли у клетки соседняя пустая клетка
    hasNeighbor: function (x, y, cmp) {
        var isEven = x % 2;
        for (var i = 0; i < Neighbor[isEven].length; i++) {
            var newX = x + Neighbor[isEven][i][0];
            var newY = y + Neighbor[isEven][i][1];
            if (newX >= 0 && newY >= 0 && newX < this.size && newY < this.size && this.table[newX][newY] == cmp) {
                return true;
            }
        }
        return false;
    },
    //существует ли слово в словаре
    isWordExists: function () {
        var findWord = Dict.indexOf(this.word.toLowerCase());
        return (findWord >= 0);
    },
    findTrieWord: function (word, cur) {
        // Get the root to start from
        cur = cur || Dict;

        // Go through every leaf
        for (var node in cur) {
            // If the start of the word matches the leaf
            if (word.indexOf(node) === 0) {
                // If it's a number
                var val = typeof cur[node] === "number" && cur[node] ?
                    // Substitute in the removed suffix object
                    Dict.$[cur[node]] :

                    // Otherwise use the current value
                    cur[node];

                // If this leaf finishes the word
                if (node.length === word.length) {
                    // Return 'true' only if we've reached a final leaf
                    return val === 0 || val.$ === 0;

                    // Otherwise continue traversing deeper
                    // down the tree until we find a match
                } else {
                    return this.findTrieWord(word.slice(node.length), val);
                }
            }
        }

        return false;
    },
    //рекурсивная функция получения 2 рядов вокруг клетки
    getNeighbors: function (x, y, level) {
        var limitLevel = 2;
        var isEven = x % 2;
        for (var i = 0; i < Neighbor[isEven].length; i++) {
            var newX = x + Neighbor[isEven][i][0];
            var newY = y + Neighbor[isEven][i][1];
            if (newX >= 0 && newY >= 0 && newX < this.size && newY < this.size) {
                var tileXY = this.table[newX][newY];
                if (typeof tileXY == 'object' && this.neighborRemove.indexOf(tileXY) == -1 && this.components.indexOf(tileXY) == -1
                ) {
                    tileXY.hasWordSprite();
                    this.neighborRemove.push(tileXY);
                }
                if (level < limitLevel) {
                    this.getNeighbors(newX, newY, level + 1);
                }
            }
        }
    },
    /**
     * Stops all running actions and schedulers
     */
    cleanup: function () {
        for (var i in this.listeners) {
            cc.eventManager.removeListener(this.listeners[i]);
            //cc.eventManager.removeCustomListeners()
        }
    },
    initListeners: function () {
        var i;
        this.listeners.push(cc.eventManager.addCustomListener(EVENT_NEW_WORD, function (data) {
            this.word = '';
            this.components = [];
            //сохраняем последний гексагон
            this.lastComponent = data.getUserData();
            //добавляем букву к слову
            this.word += this.lastComponent.letter;
            //добавляем гексагон в массив
            this.components.push(this.lastComponent);
            this.lastComponent.selectSprite();
            cc.eventManager.dispatchCustomEvent(EVENT_UPDATE_WORD, {word: this.word, exists: false});
        }.bind(this)));
        this.listeners.push(cc.eventManager.addCustomListener(EVENT_END_WORD, function () {
            if (!TouchBeginTile) {
                return;
            }
            //проверяем, что компонент касается середины
            var hasAsterisk = false;
            for (i in this.components) {
                if (this.hasNeighbor(this.components[i].tableX, this.components[i].tableY, '*')) {
                    hasAsterisk = true;
                    break;
                }
            }
            //слово есть в словаре
            var wordExists;
            if (this instanceof GameTutorial) {
                wordExists = this.isWordExists();
            } else {
                wordExists = this.findTrieWord(this.word.toLowerCase());
            }
            if (hasAsterisk && wordExists) {
                this.addWord();
                var gameEnd = this.isGameEnd(this.components);
                var removed = 0;
                //удаляем все элементы
                for (i in this.components) {
                    this.neighborRemove = [];
                    //если удаляемый элемент - бомба, удаляем близ лежащие клетки
                    if (this.components[i].isBomb) {
                        this.getNeighbors(this.components[i].tableX, this.components[i].tableY, 1);
                        cc.eventManager.dispatchCustomEvent(EVENT_HEXABOMB_REMOVED);
                    }
                    this.components[i].removeSprite();
                    removed++;
                    if (this.neighborRemove.length > 0 && !gameEnd) {
                        gameEnd = this.isGameEnd(this.neighborRemove);
                    }
                    for (var k in this.neighborRemove) {
                        if (this.neighborRemove[k].isBomb) {
                            cc.eventManager.dispatchCustomEvent(EVENT_NEIGHBOR_HEXABOMB_REMOVED);
                        }
                        this.neighborRemove[k].removeSprite();
                        removed++;
                    }
                }
                cc.eventManager.dispatchCustomEvent(EVENT_CENTER_SCENE, this.components[i]);
                if (removed > 0) {
                    cc.eventManager.dispatchCustomEvent(EVENT_TILES_REMOVED, removed);
                }
                if (gameEnd) {
                    cc.eventManager.dispatchCustomEvent(EVENT_GAME_END, true);
                }
            } else {
                if (!hasAsterisk) {
                    cc.eventManager.dispatchCustomEvent(EVENT_SHOW_INFO, t('The word must touch a cleared tile'));
                }
                for (i in this.components) {
                    this.components[i].resetSprite();
                }
            }
            cc.eventManager.dispatchCustomEvent(EVENT_UPDATE_WORD, {word: this.word, exists: wordExists});
        }.bind(this)));
        this.listeners.push(cc.eventManager.addCustomListener(EVENT_ADD_LETTER, function (data) {
            //если мы уже перемещались по этому гексагону, пропускаем итерацию
            if (this.lastComponent == data.getUserData() || !this.hasNeighbor(this.lastComponent.tableX, this.lastComponent.tableY, data.getUserData())
            ) {
                return;
            }
            //сохраняем последний гексагон
            this.lastComponent = data.getUserData();
            //добавляем букву к слову
            this.word += this.lastComponent.letter;
            //проверяем был ли уже такой гексагон
            var cmpIndex = this.components.indexOf(this.lastComponent);
            if (cmpIndex >= 0) {
                for (i = cmpIndex + 1; i < this.components.length; i++) {
                    this.components[i].resetSprite();
                }
                this.word = this.word.substring(0, cmpIndex + 1);
                this.components = this.components.slice(0, cmpIndex + 1);
            } else {
                //добавляем гексагон в массив
                this.components.push(this.lastComponent);
            }
            this.lastComponent.selectSprite();
            //проверяем, что компонент касается середины
            var hasAsterisk = false;
            for (i in this.components) {
                if (this.hasNeighbor(this.components[i].tableX, this.components[i].tableY, '*')) {
                    hasAsterisk = true;
                    break;
                }
            }
            var wordExists;
            if (this instanceof GameTutorial) {
                wordExists = this.isWordExists();
            } else {
                wordExists = this.findTrieWord(this.word.toLowerCase());
            }
            if (hasAsterisk && wordExists) {
                for (i in this.components) {
                    this.components[i].hasWordSprite();
                }
            } else {
                for (i in this.components) {
                    this.components[i].selectSprite();
                }
            }
            cc.eventManager.dispatchCustomEvent(EVENT_UPDATE_WORD, {word: this.word, exists: wordExists});
        }.bind(this)));

        this.listeners.push(cc.eventManager.addCustomListener(EVENT_TILE_REMOVE, function (data) {
            var userData = data.getUserData();
            var tile = this.table[userData.x][userData.y];
            if (!this.gameEnded) {
                this.addScoreForLetter(tile.letter);
                //покажем кол-во заработанных очков за каждую найденную букву
                var score = this.getScoreForLetter(tile.letter);
                cc.eventManager.dispatchCustomEvent(EVENT_FLASH_SCORE, {score: score, x: tile.x, y: tile.y});
            }
            if (tile != '*') {
                tile.removeAllChildren(true);
                tile.removeFromParent();
            }
            this.table[userData.x][userData.y] = '*';
            this.tilesRemoved++;
        }.bind(this)));

        this.listeners.push(cc.eventManager.addCustomListener(EVENT_TIMER_END, function () {
            //this.gameEnd(false);
            cc.eventManager.dispatchCustomEvent(EVENT_GAME_END, false);
        }.bind(this)));

        this.listeners.push(cc.eventManager.addCustomListener(EVENT_GAME_END, function (data) {
            var win = data.getUserData();
            this.gameEnd(win);
        }.bind(this)));

    },
    getResults: function () {

    }
});