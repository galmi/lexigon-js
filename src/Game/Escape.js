var GameEscape = GameAbstract.extend({
    hexagon_normal: "escape_hexagon_normal.png",
    hexagon_selected: "escape_hexagon_selected.png",
    hexagon_white: "escape_hexagon_white.png",
    hexagon_border: "escape_hexagon_border.png",
    hexagon_bomb: "escape_bomb.png",
    pauseButton: "escape_pause.png",
    fontColor: cc.color(51, 153, 204, 255),
    size: 63,
    //ctor: function() {
    //    this.size = 63;
    //},
    isGameEnd: function(components) {
        //слово собрано, проверяем - нашли ли мы выход
        for (i in components) {
            if (this.hasNeighborExit(components[i].tableX, components[i].tableY)) {
                return true;
            }
        }
        return false;
    }
});

GameEscape.gamesLimit = 1;

GameEscape.create = function () {
    var obj = new GameEscape();
    if (obj && obj.init()) {
        return obj;
    }
    return null;
};