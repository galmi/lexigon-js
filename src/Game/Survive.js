var GameSurvive = GameAbstract.extend({
    surviveTime: 0,
    hexagon_normal: "survive_hexagon_normal.png",
    hexagon_selected: "survive_hexagon_selected.png",
    hexagon_white: "survive_hexagon_white.png",
    hexagon_border: "survive_hexagon_border.png",
    hexagon_bomb: "survive_bomb.png",
    pauseButton: "survive_pause.png",
    size: 63,
    ctor: function () {
        this.listeners.push(cc.eventManager.addCustomListener(EVENT_TILES_REMOVED, function (data) {
            var count = data.getUserData();
            this.surviveTime += count;
            cc.eventManager.dispatchCustomEvent(EVENT_TIMER_ADD, count);
        }.bind(this)));
        this.listeners.push(cc.eventManager.addCustomListener(EVENT_TIMER_START, function(data) {
            this.surviveTime += data.getUserData();
        }.bind(this)));
    }
});

GameSurvive.gamesLimit = 80;

GameSurvive.create = function () {
    var obj = new GameSurvive();
    if (obj && obj.init()) {
        return obj;
    }
    return null;
};