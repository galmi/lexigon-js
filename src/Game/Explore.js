var GameExplore = GameAbstract.extend({
    hexagon_normal: "explore_hexagon_normal.png",
    hexagon_selected: "explore_hexagon_selected.png",
    hexagon_white: "explore_hexagon_white.png",
    hexagon_border: "explore_hexagon_border.png",
    hexagon_bomb: "explore_bomb.png",
    pauseButton: "explore_pause.png",
    endButton: "explore_finish_icon.png",
    fontColor: cc.color(153, 204, 51, 255),
    size: 19
});

GameExplore.gamesLimit = 30;

GameExplore.create = function () {
    var obj = new GameExplore();
    if (obj && obj.init()) {
        return obj;
    }
    return null;
};