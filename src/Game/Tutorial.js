var GameTutorial = GameAbstract.extend({
    gamesLimit: 0,
    currentQuest: 1,
    hexagon_normal: "tutorial_hexagon_normal.png",
    hexagon_white: "tutorial_hexagon_white.png",
    hexagon_selected: "tutorial_hexagon_selected.png",
    hexagon_border: "tutorial_hexagon_border.png",
    hexagon_bomb: "tutorial_bomb.png",
    size: 11,
    initTable: function() {
        var table = getDictValue('tutorialTable');
        var i;
        var tileParams = {
            hexagon_normal: this.hexagon_normal,
            hexagon_selected: this.hexagon_selected,
            hexagon_white: this.hexagon_white,
            hexagon_bomb: this.hexagon_bomb,
            hexagon_border: this.hexagon_border
        };
        BOMB_LETTERS = getDictValue('tutorialBombLetters');
        for (i = 0; i < this.size; i++) {
            this.table[i] = [];
            for (var j = 0; j < this.size; j++) {
                var letter = table[i][j];
                if (letter == null) {
                    continue;
                } else if (letter == '*') {
                    this.table[i][j] = '*';
                } else {
                    this.table[i][j] = TileSprite.create(i, j, letter, tileParams);
                }
                this.tilesCount++;
            }
        }
        this.tileWidth = this.table[0][5].width;
        this.tileHeight = this.table[0][5].height;

        return this.table;
    }
});

GameTutorial.create = function () {
    var obj = new GameTutorial();
    if (obj && obj.init()) {
        return obj;
    }
    return null;
};