var achievements = [
    {
        id: 'the_graduate',
        name: t('The Graduate'),
        description: t('You completed Tutorial'),
        game: 'Tutorial',
        icon: 'graduate.png',
        pts: 1,
        init: function () {
            var event = cc.eventManager.addCustomListener(EVENT_TUTORIAL_END, function () {
                sendAchieve(this, event);
            }.bind(this));
        }
    }, {
        id: '3lw',
        name: t('%d letters word', 3),
        description: t('You made %s letters word', 3),
        icon: '3_letters_word.png',
        lettersCount: 3,
        pts: 1,
        init: function () {
            checkLettersCountWord(this);
        }
    }, {
        id: '4lw',
        name: t('%d letters word', 4),
        description: t('You made %s letters word', 4),
        icon: '4_letters_word.png',
        lettersCount: 4,
        pts: 3,
        init: function () {
            checkLettersCountWord(this);
        }
    }, {
        id: '5lw',
        name: t('%d letters word', 5),
        description: t('You made %s letters word', 5),
        icon: '5_letters_word.png',
        lettersCount: 5,
        pts: 5,
        init: function () {
            checkLettersCountWord(this);
        }
    }, {
        id: '6lw',
        name: t('%d letters word', 6),
        description: t('You made %s letters word', 6),
        icon: '6_letters_word.png',
        lettersCount: 6,
        pts: 10,
        init: function () {
            checkLettersCountWord(this);
        }
    }, {
        id: '7lw',
        name: t('%d letters word', 7),
        description: t('You made %s letters word', 7),
        icon: '7_letters_word.png',
        lettersCount: 7,
        pts: 20,
        init: function () {
            checkLettersCountWord(this);
        }
    }, {
        id: '8lw',
        name: t('%d letters word', 8),
        description: t('You made %s letters word', 8),
        icon: '8_letters_word.png',
        lettersCount: 8,
        pts: 30,
        init: function () {
            checkLettersCountWord(this);
        }
    }, {
        id: '9lw',
        name: t('%d letters word', 9),
        description: t('You made %s letters word', 9),
        icon: '9_letters_word.png',
        lettersCount: 9,
        pts: 40,
        init: function () {
            checkLettersCountWord(this);
        }
    }, {
        id: '10lw',
        name: t('%d letters word', 10),
        description: t('You made %s letters word', 10),
        icon: '10_letters_word.png',
        lettersCount: 10,
        pts: 50,
        init: function () {
            checkLettersCountWord(this);
        }
    }, {
        id: '10G',
        name: t('The %d games', 10),
        description: t('You played %s games', 10),
        icon: '10_games.png',
        gamesCount: 10,
        pts: 1,
        init: function () {
            checkGamesCount(this);
        }
    }, {
        id: '100G',
        name: t('The %d games', 100),
        description: t('You played %s games', 100),
        icon: '100_games.png',
        gamesCount: 100,
        pts: 5,
        init: function () {
            checkGamesCount(this);
        }
    }, {
        id: '250G',
        name: t('The %d games', 250),
        description: t('You played %s games', 250),
        icon: '250_games.png',
        gamesCount: 250,
        pts: 10,
        init: function () {
            checkGamesCount(this);
        }
    }, {
        id: '500G',
        name: t('The %d games', 500),
        description: t('You played %s games', 500),
        icon: '500_games.png',
        gamesCount: 500,
        pts: 20,
        init: function () {
            checkGamesCount(this);
        }
    }, {
        id: '1000G',
        name: t('The %d games', 1000),
        description: t('You played %s games', 1000),
        icon: '1000_games.png',
        gamesCount: 1000,
        pts: 50,
        init: function () {
            checkGamesCount(this);
        }
    }, {
        id: 'hexabomber',
        name: t('Hexabomber'),
        description: t('You used a hexabomb'),
        icon: 'hexabomber.png',
        pts: 2,
        init: function () {
            var event = cc.eventManager.addCustomListener(EVENT_HEXABOMB_REMOVED, function (data) {
                if (!(cc.director.getRunningScene().game instanceof GameTutorial)) {
                    sendAchieve(this, event);
                }
            }.bind(this));
        }
    }, {
        id: 'big_bang',
        name: t('The Big Bang'),
        description: t('Make a word that uses 2 hexabombs'),
        icon: 'hexabomber.png',
        pts: 5,
        init: function () {
            var event = cc.eventManager.addCustomListener(EVENT_ADDED_WORD, function (data) {
                var word = data.getUserData();
                var hexabombs = 0;
                for (var i = 0; i < word.length; i++) {
                    if (BOMB_LETTERS.indexOf(word[i]) != -1) {
                        hexabombs++;
                    }
                }
                if (hexabombs == 2) {
                    sendAchieve(this, event);
                }
            }.bind(this));
        }
    }, {
        id: 'drive_by_hexabomber',
        name: t('Drive By Hexabomber'),
        description: t('You exploded a hexabomb with another hexabomb'),
        icon: 'hexabomber.png',
        pts: 5,
        init: function () {
            var event = cc.eventManager.addCustomListener(EVENT_NEIGHBOR_HEXABOMB_REMOVED, function () {
                sendAchieve(this, event);
            }.bind(this));
        }
    }, {
        id: 'four_plus_four',
        name: t('Four Plus Four'),
        description: t('Find the same 4+ letters word consecutively'),
        icon: 'four_plus_four.png',
        lastWord: null,
        pts: 10,
        init: function () {
            var event = cc.eventManager.addCustomListener(EVENT_ADDED_WORD, function (data) {
                var word = data.getUserData();
                if (this.lastWord == word) {
                    sendAchieve(this, event);
                } else {
                    this.lastWord = word;
                }
            }.bind(this));
        }
    }, {
        id: 'you_escaped',
        name: t('You Escaped'),
        description: t('You escaped for the first time'),
        icon: 'you_escaped.png',
        game: 'Escape',
        pts: 2,
        init: function () {
            var event = cc.eventManager.addCustomListener(EVENT_SEND_STAT, function (data) {
                var userData = data.getUserData();
                if (userData.gameType == 'Escape' && !!userData.win) {
                    sendAchieve(this, event);
                }
            }.bind(this));
        }
    }, {
        id: 'two_minute_escape',
        name: t('Two Minutes Escape'),
        description: t('You escaped in under %2 minutes'),
        icon: 'two_minute_escape.png',
        game: 'Escape',
        pts: 1,
        escapeTime: 120,
        init: function () {
            escapedUnder(this);
        }
    }, {
        id: 'one_minute_escape',
        name: t('One Minute Escape'),
        description: t('You escaped in under 1 minute'),
        icon: 'one_minute_escape.png',
        game: 'Escape',
        pts: 2,
        escapeTime: 60,
        init: function () {
            escapedUnder(this);
        }
    }, {
        id: '50_second_escape',
        name: t('%d Seconds Escape', 50),
        description: t('You escaped in under %s seconds', 50),
        icon: '50_second_escape.png',
        game: 'Escape',
        pts: 5,
        escapeTime: 50,
        init: function () {
            escapedUnder(this);
        }
    }, {
        id: '40_second_escape',
        name: t('%d Seconds Escape', 40),
        description: t('You escaped in under %s seconds', 40),
        icon: '40_second_escape.png',
        game: 'Escape',
        pts: 10,
        escapeTime: 40,
        init: function () {
            escapedUnder(this);
        }
    }, {
        id: '30_second_escape',
        name: t('%d Seconds Escape', 30),
        description: t('You escaped in under %s seconds', 30),
        icon: '30_second_escape.png',
        game: 'Escape',
        pts: 20,
        escapeTime: 30,
        init: function () {
            escapedUnder(this);
        }
    }, {
        id: '20_second_escape',
        name: t('%d Seconds Escape', 20),
        description: t('You escaped in under %s seconds', 20),
        icon: '20_second_escape.png',
        game: 'Escape',
        pts: 50,
        escapeTime: 20,
        init: function () {
            escapedUnder(this);
        }
    }, {
        id: '3_game_streak',
        name: t('%d Games Strike', 3),
        description: t('You escaped %s times in a row', 3),
        icon: '3_escape_strike.png',
        game: 'Escape',
        pts: 1,
        streak: 3,
        init: function () {
            escapedStreak(this);
        }
    }, {
        id: '5_game_streak',
        name: t('%d Games Strike', 5),
        description: t('You escaped %s times in a row', 5),
        icon: '5_escape_strike.png',
        game: 'Escape',
        pts: 2,
        streak: 5,
        init: function () {
            escapedStreak(this);
        }
    }, {
        id: '10_game_streak',
        name: t('%d Games Strike', 10),
        description: t('You escaped %s times in a row', 10),
        icon: '10_escape_strike.png',
        game: 'Escape',
        pts: 4,
        streak: 10,
        init: function () {
            escapedStreak(this);
        }
    }, {
        id: '20_game_streak',
        name: t('%d Games Strike', 20),
        description: t('You escaped %s times in a row', 20),
        icon: '20_escape_strike.png',
        game: 'Escape',
        pts: 10,
        streak: 20,
        init: function () {
            escapedStreak(this);
        }
    }, {
        id: 'high_society',
        name: t('High Society'),
        description: t('You escaped using 5+ letters word'),
        icon: 'high_society.png',
        game: 'Escape',
        pts: 5,
        word: null,
        init: function () {
            var event = cc.eventManager.addCustomListener(EVENT_ADDED_WORD, function (data) {
                this.word = data.getUserData();
            }.bind(this));
            var event2 = cc.eventManager.addCustomListener(EVENT_SEND_STAT, function (data) {
                var userData = data.getUserData();
                if (userData.gameType == 'Escape' && userData.win && this.word.length >= 5) {
                    sendAchieve(this, event);
                    cc.eventManager.removeListener(event2);
                }
            }.bind(this));
        }
    }, {
        id: 'hexagone',
        name: t('Hexagone'),
        description: t('You escaped using hexabomb'),
        icon: 'hexagone.png',
        game: 'Escape',
        pts: 2,
        word: null,
        init: function () {
            var event = cc.eventManager.addCustomListener(EVENT_ADDED_WORD, function (data) {
                this.word = data.getUserData();
            }.bind(this));
            var event2 = cc.eventManager.addCustomListener(EVENT_SEND_STAT, function (data) {
                var userData = data.getUserData();
                if (userData.gameType == 'Escape' && userData.win) {
                    var hexabombs = 0;
                    for (var i = 0; i < this.word.length; i++) {
                        if (BOMB_LETTERS.indexOf(this.word[i]) != -1) {
                            hexabombs++;
                        }
                    }
                    if (hexabombs > 0) {
                        sendAchieve(this, event);
                        cc.eventManager.removeListener(event2);
                    }
                }
            }.bind(this));
        }
    }, {
        id: 'the_5k_club',
        name: t('The %dK Club', 5),
        description: t('Score %s in Explore mode', '5,000'),
        icon: 'the_5k_club.png',
        game: 'Explore',
        pts: 5,
        score: 5000,
        init: function () {
            exploreScore(this);
        }
    }, {
        id: 'the_10k_club',
        name: t('The %dK Club', 10),
        description: t('Score %s in Explore mode', '10,000'),
        icon: 'the_10k_club.png',
        game: 'Explore',
        pts: 10,
        score: 10000,
        init: function () {
            exploreScore(this);
        }
    }, {
        id: 'the_15k_club',
        name: t('The %dK Club', 15),
        description: t('Score %s in Explore mode', '15,000'),
        icon: 'the_15k_club.png',
        game: 'Explore',
        pts: 25,
        score: 15000,
        init: function () {
            exploreScore(this);
        }
        //}, {
        //    id: '666',
        //    name: 'Hellish',
        //    description: 'Score a devilish number 666',
        //    icon: '666.png',
        //    game: 'Explore',
        //    pts: 1,
        //    score: 666,
        //    init: function () {
        //        var event = cc.eventManager.addCustomListener(EVENT_SEND_STAT, function (data) {
        //            var userData = data.getUserData();
        //            if (userData.gameType == 'Explore' && userData.score == this.score) {
        //                achieveInfoBox(this.icon, this.name);
        //                cc.eventManager.removeListener(event);
        //                cc.eventManager.dispatchCustomEvent(EVENT_SEND_ACHIEVEMENT, this.id);
        //            }
        //        }.bind(this));
        //    }
    }, {
        id: 'the_1m_club',
        name: t('The %dM Club', 1),
        description: t('You survived for 1 minute'),
        icon: 'the_1m_club.png',
        game: 'Survive',
        pts: 1,
        surviveTime: 60,
        init: function () {
            surviveTime(this);
        }
    }, {
        id: 'the_2m_club',
        name: t('The %dM Club', 2),
        description: t('You survived for %s minutes', 2),
        icon: 'the_2m_club.png',
        game: 'Survive',
        pts: 2,
        surviveTime: 120,
        init: function () {
            surviveTime(this);
        }
    }, {
        id: 'the_3m_club',
        name: t('The %dM Club', 3),
        description: t('You survived for %s minutes', 3),
        icon: 'the_3m_club.png',
        game: 'Survive',
        pts: 3,
        surviveTime: 180,
        init: function () {
            surviveTime(this);
        }
    }, {
        id: 'the_4m_club',
        name: t('The %dM Club', 4),
        description: t('You survived for %s minutes', 4),
        icon: 'the_4m_club.png',
        game: 'Survive',
        pts: 4,
        surviveTime: 240,
        init: function () {
            surviveTime(this);
        }
    }, {
        id: 'the_5m_club',
        name: t('The %dM Club', 5),
        description: t('You survived for %s minutes', 5),
        icon: 'the_5m_club.png',
        game: 'Survive',
        pts: 5,
        surviveTime: 300,
        init: function () {
            surviveTime(this);
        }
    }, {
        id: 'the_6m_club',
        name: t('The %dM Club', 6),
        description: t('You survived for %s minutes', 6),
        icon: 'the_6m_club.png',
        game: 'Survive',
        pts: 6,
        surviveTime: 360,
        init: function () {
            surviveTime(this);
        }
    }, {
        id: 'the_7m_club',
        name: t('The %dM Club', 7),
        description: t('You survived for %s minutes', 7),
        icon: 'the_7m_club.png',
        game: 'Survive',
        pts: 7,
        surviveTime: 420,
        init: function () {
            surviveTime(this);
        }
    }, {
        id: 'the_8m_club',
        name: t('The %dM Club', 8),
        description: t('You survived for %s minutes', 8),
        icon: 'the_8m_club.png',
        game: 'Survive',
        pts: 8,
        surviveTime: 480,
        init: function () {
            surviveTime(this);
        }
    }, {
        id: 'the_9m_club',
        name: t('The %dM Club', 9),
        description: t('You survived for %s minutes', 9),
        icon: 'the_9m_club.png',
        game: 'Survive',
        pts: 9,
        surviveTime: 540,
        init: function () {
            surviveTime(this);
        }
    }, {
        id: 'the_10m_club',
        name: t('The %dM Club', 10),
        description: t('You survived for %s minutes', 10),
        icon: 'the_10m_club.png',
        game: 'Survive',
        pts: 10,
        surviveTime: 600,
        init: function () {
            surviveTime(this);
        }
    }, {
        id: 'the_11m_club',
        name: t('The %dM Club', 11),
        description: t('You survived for %s minutes', 11),
        icon: 'the_11m_club.png',
        game: 'Survive',
        pts: 15,
        surviveTime: 660,
        init: function () {
            surviveTime(this);
        }
    }, {
        id: 'the_12m_club',
        name: t('The %dM Club', 12),
        description: t('You survived for %s minutes', 12),
        icon: 'the_12m_club.png',
        game: 'Survive',
        pts: 20,
        surviveTime: 720,
        init: function () {
            surviveTime(this);
        }
    }, {
        id: 'the_13m_club',
        name: t('The %dM Club', 13),
        description: t('You survived for %s minutes', 13),
        icon: 'the_13m_club.png',
        game: 'Survive',
        pts: 25,
        surviveTime: 780,
        init: function () {
            surviveTime(this);
        }
    }, {
        id: 'the_14m_club',
        name: t('The %dM Club', 14),
        description: t('You survived for %s minutes', 14),
        icon: 'the_14m_club.png',
        game: 'Survive',
        pts: 30,
        surviveTime: 840,
        init: function () {
            surviveTime(this);
        }
    }, {
        id: 'the_15m_club',
        name: t('The %dM Club', 15),
        description: t('You survived for %s minutes', 15),
        icon: 'the_15m_club.png',
        game: 'Survive',
        pts: 35,
        surviveTime: 900,
        init: function () {
            surviveTime(this);
        }
    }, {
        id: 'smashing_it',
        name: t('Smashing it'),
        description: t('Bump your remaining time to 60 seconds in Survive mode'),
        icon: 'smashing_it.png',
        game: 'Survive',
        pts: 10,
        init: function () {
            var event = cc.eventManager.addCustomListener(EVENT_TIMER, function (data) {
                var timer = data.getUserData();
                if (timer > 60) {
                    sendAchieve(this, event);
                }
            }.bind(this));
        }
    }
];

cc.eventManager.addCustomListener(EVENT_GAME_LOADED, function () {
    for (var i in achievements) {
        if (!cc.UserData.achievements || cc.UserData.achievements.indexOf(achievements[i].id) == -1) {
            achievements[i].init();
        }
    }

    cc.eventManager.addCustomListener(EVENT_SEND_ACHIEVEMENT, function(data) {
        var achieveData = data.getUserData();
        if (!cc.UserData.achievements) {
            cc.UserData.achievements = [];
        }
        cc.UserData.achievements.push(achieveData.id);
    })
});

/**
 * Количество сыгранных игр
 * @param obj
 */
function checkGamesCount(obj) {
    var event = cc.eventManager.addCustomListener(EVENT_SEND_STAT, function (data) {
        if (cc.UserData['gamesPlayed'] >= obj.gamesCount) {
            sendAchieve(this, event);
        }
    }.bind(obj));
}

function exploreScore(obj) {
    var event = cc.eventManager.addCustomListener(EVENT_SEND_STAT, function (data) {
        var userData = data.getUserData();
        if (userData.gameType == 'Explore' && userData.score >= this.score) {
            sendAchieve(this, event);
        }
    }.bind(obj));
}

/**
 * Длина найденных слов
 * @param obj
 */
function checkLettersCountWord(obj) {
    var event = cc.eventManager.addCustomListener(EVENT_ADDED_WORD, function (data) {
        if (!(cc.director.getRunningScene().game instanceof GameTutorial)) {
            var word = data.getUserData();
            if (word.length == obj.lettersCount) {
                sendAchieve(this, event);
            }
        }
    }.bind(obj));
}

/**
 * Время завершения игры Выживание
 * @param obj
 */
function escapedUnder(obj) {
    var event = cc.eventManager.addCustomListener(EVENT_SEND_STAT, function (data) {
        var userData = data.getUserData();
        if (userData.gameType == 'Escape' && userData.time <= this.escapeTime) {
            sendAchieve(this, event);
        }
    }.bind(obj));
}

function surviveTime(obj) {
    var event = cc.eventManager.addCustomListener(EVENT_SEND_STAT, function (data) {
        var userData = data.getUserData();
        if (userData.gameType == 'Survive' && userData.time >= this.surviveTime) {
            sendAchieve(this, event);
        }
    }.bind(obj));
}

function escapedStreak(obj) {
    var event = cc.eventManager.addCustomListener(EVENT_SEND_STAT, function (data) {
        var userData = data.getUserData();
        if (userData.gameType == 'Escape' && userData.win == true && cc.UserData['escapeStreak'] >= obj.streak) {
            sendAchieve(this, event);
        }
    }.bind(obj));
}

/**
 * Выводит всплывающее уведомление о достижении
 * @param icon
 * @param name
 */
function achieveInfoBox(icon, name) {
    var size = cc.director.getWinSize();
    var layerWidth = 350;
    var layerHeight = 82;
    var layer = new cc.LayerColor(cc.color(0, 0, 0, 255), layerWidth, layerHeight);
    layer.setOpacity(200);
    layer.setZOrder(1000);
    var iconSprite = new cc.Sprite('#' + icon);
    iconSprite.setPosition(iconSprite.width / 2 + 10, layer.height / 2);
    layer.addChild(iconSprite);
    var text = t("New Achievement");
    var label = new cc.LabelTTF(text, 'Arial', 20);
    label.setPositionX(iconSprite.x + iconSprite.width / 2 + label.width / 2 + 10);
    label.setPositionY(layer.height / 2 + label.height/2);
    layer.addChild(label);
    var achieveLabel = new cc.LabelBMFont(name, res.FntIntro20White);
    achieveLabel.setColor(cc.color(39, 170, 225));
    achieveLabel.setPositionX(iconSprite.x + iconSprite.width / 2 + achieveLabel.width / 2 + 10);
    achieveLabel.setPositionY(layer.height / 2 - achieveLabel.height/2);
    layer.addChild(achieveLabel);

    var layerX = size.width - layer.width;
    layer.setPositionX(layerX);
    layer.setPositionY(size.height - 1);

    cc.director.getRunningScene().addChild(layer);

    var show = new cc.MoveTo(.5, cc.p(layerX, size.height - layer.height));
    var pause = cc.actionInterval(2);
    pause.update = function () {
    };
    var hide = new cc.MoveTo(.5, cc.p(layerX, size.height + layer.height));
    var del = new cc.RemoveSelf(true);
    layer.runAction(cc.sequence(show, pause, hide, del));
}

function sendAchieve(obj, event) {
    achieveInfoBox(obj.icon, obj.name);
    cc.eventManager.removeListener(event);
    cc.eventManager.dispatchCustomEvent(EVENT_SEND_ACHIEVEMENT, {id: obj.id, pts: obj.pts});
}
