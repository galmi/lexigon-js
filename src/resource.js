var EVENT_GAME_START = 'game_start';
var EVENT_GAME_PAUSED = 'game_paused';
var EVENT_GAME_RESUMED = 'game_resumed';
var EVENT_GAME_END = 'game_end';
var EVENT_NEW_GAME = 'new_game';
var EVENT_NEW_WORD = 'new_word';
var EVENT_ADD_LETTER = 'add_letter';
var EVENT_UPDATE_WORD = 'update_word';
var EVENT_END_WORD = 'end_word';
var EVENT_ADDED_WORD = 'added_word';
var EVENT_ADDED_SCORE = 'added_score';
var EVENT_UPDATED_SCORE = 'updated_score';
var EVENT_SHOW_INFO = 'show_info';
var EVENT_TIMER_START = 'timer_start';
var EVENT_TIMER_STOP = 'timer_stop';
var EVENT_TIMER_PAUSE = 'timer_pause';
var EVENT_TIMER_CONTINUE = 'timer_continue';
var EVENT_TIMER_END = 'timer_end';
var EVENT_TIMER_ADD = 'timer_add';
var EVENT_TIMER = 'timer';
var EVENT_TILE_REMOVE = 'tile_remove';          //удален тайл
var EVENT_TILES_REMOVED = 'tiles_removed';      //за последний ход удалено N тайлов
var EVENT_PAY = 'pay';
var EVENT_SEND_STAT = 'send_stat';              //отправить статистику
var EVENT_UPDATE_STAT = 'update_stat';          //обновить статистику
var EVENT_LOADED_STAT = 'loaded_stat';          //загружена статистика
var EVENT_TOUCH_MOVED = 'touch_moved';          //было перемещение поля
var EVENT_TUTORIAL_END = 'tutorial_end';          //обучение закончено
var EVENT_GAME_LOADED = 'game_loaded';
var EVENT_MAIN_MENU_ADD_CHILD = 'main_menu_add_child';
var EVENT_MAIN_MENU_LOADED = 'main_menu_loaded';
var EVENT_DICTIONARY_CHANGED = 'dictionary_changed';
var EVENT_SEND_ACHIEVEMENT = 'send_achievement';
var EVENT_FLASH_SCORE = 'flash_score';
var EVENT_HEXABOMB_REMOVED = 'hexabomb_removed';
var EVENT_NEIGHBOR_HEXABOMB_REMOVED = 'neighbor_hexabomb_removed';
var EVENT_SHOW_HINT = 'show_hint';
var EVENT_HINT_FOUNDED = 'hint_founded';
var EVENT_BUY_HINTS_MENU = 'buy_hints_menu';
var EVENT_BUY_HINTS = 'buy_hints';
var EVENT_HINTS_ADDED = 'hints_added';
var EVENT_HINTS_UPDATE = 'hints_update';
var EVENT_CENTER_SCENE = 'center_scene';

var SEX_FEMALE = 1;
var SEX_MALE = 2;

cc.UserData = {};
cc.Social = null;
cc.Statistic = {};

setUserLang();
setUserDict();

var LETTERS_FREQ = getDictValue('lettersFreq');
var BOMB_LETTERS = getDictValue('bombLetters');
//соседние клетки для четных и нечетных клеток
var Neighbor = [
    [[-1, -1], [0, -1], [+1, -1],
        [-1, 0], [0, +1], [+1, 0]],

    [[-1, 0], [0, -1], [+1, 0],
        [-1, +1], [0, +1], [+1, +1]]
];

cc.hexagonContainsPoint = function (rect, point) {
    var sign = function (p1, p2, p3) {
        return (p1.x - p3.x) * (p2.y - p3.y) - (p2.x - p3.x) * (p1.y - p3.y);
    };

    var pointInTriangle = function (pt, v1, v2, v3) {
        var b1, b2, b3;

        b1 = sign(pt, v1, v2) < 0.0;
        b2 = sign(pt, v2, v3) < 0.0;
        b3 = sign(pt, v3, v1) < 0.0;

        return ((b1 == b2) && (b2 == b3));
    };
    if (cc.rectContainsPoint(rect, point)) {
        var width = cc.rectGetMaxY(rect) / 2;
        var inHexRect = cc.rect(width / 2, 0, width, cc.rectGetMaxY(rect));
        if (cc.rectContainsPoint(inHexRect, point)) {
            return true;
        }
        var mDot, tDot, bDot;
        //левый треугольник
        mDot = cc.p(0, cc.rectGetMidY(rect));
        tDot = cc.p(cc.rectGetMinX(inHexRect), 0);
        bDot = cc.p(cc.rectGetMinX(inHexRect), cc.rectGetMaxY(inHexRect));
        if (pointInTriangle(point, mDot, tDot, bDot)) {
            return true;
        }
        //правый теругольник
        mDot = cc.p(cc.rectGetMaxX(rect), cc.rectGetMidY(rect));
        tDot = cc.p(cc.rectGetMaxX(inHexRect), 0);
        bDot = cc.p(cc.rectGetMaxX(inHexRect), cc.rectGetMaxY(inHexRect));
        if (pointInTriangle(point, mDot, tDot, bDot)) {
            return true;
        }
    }
    return false;
};

var TouchBeginTile = false;

if (typeof CURRENCY == "undefined") {
    var CURRENCY = '';
}

var res = {
    Dict_json: getDictValue('dict'),
    Game_png: "res/game.png?3",
    Game_plist: "res/game.plist?3",
    Menu_png: "res/menu.png?3",
    Menu_plist: "res/menu.plist?3",
    Achieve_png: "res/achievements.png?3",
    Achieve_plist: "res/achievements.plist?3",
    Background_png: "res/background.png",
    FntIntro25Gray: 'res/Intro-25-gray.fnt',
    FntIntro20White: 'res/Intro-20-white.fnt',
    FntIntro40White: 'res/Intro-40-white.fnt',
    FntIntro40Gray: 'res/Intro-40-gray.fnt',
    escape_title: t('Escape'),
    escape_description: t('Time limited game. Find exit as soon as possible.'),
    escape_description_locked: t('Time limited game. Find exit as soon as possible.') + "\n" + t('The game will be unlocked after Tutorial.'),
    explore_title: t('Explore'),
    explore_description: t('The game without limits.') + "\n" + t('Find the largest number of words.'),
    explore_description_locked: t('The game without limits.') + "\n" + t('Unlocked after %d games played, or after payment %s. Click for pay.', 30, CURRENCY),
    survive_title: t('Survive'),
    survive_description: t('The game against time. Find words for survive.'),
    survive_description_locked: t('The game against time. Find words for survive.') + "\n" + t('Unlocked after %d games played, or after payment %s. Click for pay.', 80, CURRENCY),
    tutorial_title: t('Tutorial'),
    tutorial_description: t('Play Tutorial game first.'),

    sprite: {
        menuNormal: "menu_normal.png",
        menuSelected: "menu_selected.png",
        menuBlank: "menu_blank.png",
        pauseButtonSelected: "pause_selected.png"
    },
    colors: {
        tutorial: cc.color(255, 0, 153),
        escape: cc.color(51, 153, 204, 255),
        explore: cc.color(153, 204, 51, 255),
        survive: cc.color(255, 102, 51)
    }
};

var winSize = {w: 800, h: 700};

var g_menu = [
    res.Menu_plist,
    res.Menu_png,
    res.Achieve_plist,
    res.Achieve_png,
    res.Background_png,
    res.FntIntro25Gray,
    res.FntIntro20White
];

function getGameResources() {
    return [
        res.Game_png,
        res.Game_plist,
        res.Achieve_plist,
        res.Achieve_png,
        getDictValue('dict'),
        res.FntIntro25Gray,
        res.FntIntro40White,
        res.FntIntro40Gray
    ];
}

String.prototype.repeat = function (n) {
    n = n || 1;
    return new Array(n + 1).join(this);
};

Number.prototype.toMMSS = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var minutes = Math.floor(sec_num / 60);
    var seconds = sec_num - (minutes * 60);

    if (minutes < 10) {
        minutes = "0" + minutes;
    }
    if (seconds < 10) {
        seconds = "0" + seconds;
    }
    return minutes + ':' + seconds;
};

Object.size = function (obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

/**
 * bugfix
 *
 * @param l1
 * @param l2
 * @returns {number}
 * @private
 */
//cc.eventManager._sortEventListenersOfSceneGraphPriorityDes = function (l1, l2) {
//    var locNodePriorityMap = cc.eventManager._nodePriorityMap;
//    if (!l2._getSceneGraphPriority() || !l1._getSceneGraphPriority()) {
//        return 0;
//    }
//    return locNodePriorityMap[l2._getSceneGraphPriority().__instanceId] - locNodePriorityMap[l1._getSceneGraphPriority().__instanceId];
//};

function isPaid() {
    return cc.UserData.hasOwnProperty('paid') && cc.UserData['paid'] === true;
}

/**
 * Return cross domain allowed image
 * @param url
 * @returns {XML|string|void}
 * @constructor
 */
function CDI(url) {
    var proxy = 'http://www.corsproxy.com/';
    return url.replace(/http[s]*:\/\//, proxy);
}

function loadDictFull(path) {
    (function () {
        var s = document.createElement('script');
        s.type = 'text/javascript';
        s.async = true;
        s.src = path;
        var x = document.getElementsByTagName('script')[0];
        x.parentNode.insertBefore(s, x);
    })();
}

function changeRes(width, height) {
    var resolutionPolicy;
    if (width==winSize.w && height==winSize.h) {
        resolutionPolicy = cc.ResolutionPolicy.UNKNOWN;
    } else {
        resolutionPolicy = cc.ResolutionPolicy.SHOW_ALL;
    }
    cc.view.setDesignResolutionSize(width, height, resolutionPolicy);
}